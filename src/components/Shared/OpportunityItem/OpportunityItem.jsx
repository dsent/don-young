import React from 'react';
import { Link } from "react-router-dom";
import type { Opportunity } from '../../../types/opportunity.type';

import opportunityBlockTime from '../../../assets/images/opportunity-block__time.svg';
import opportunityBlockAddress from '../../../assets/images/opportunity-block__address.svg';

export default function OpportunityItem(props: Opportunity) {
    return (
        <Link className="opportunity-list__item" to={`/opportunity/${props.id}`}>
            <div className="opportunity-list__image">
                <img src={process.env.REACT_APP_API_BASE_URL + props.image} alt=""/>
            </div>

            <div className="opportunity-list__title">{props.title}</div>

            <div className="opportunity-list__text">
                {
                    props.time && (
                        <div className="opportunity-list__text-item">
                            <div className="opportunity-list__text-icon">
                                <img src={opportunityBlockTime} alt=""/>
                            </div>
                            <span>{props.time}</span>
                        </div>
                    )
                }

                {
                    props.address && (
                        <div className="opportunity-list__text-item">
                            <div className="opportunity-list__text-icon">
                                <img src={opportunityBlockAddress} alt=""/>
                            </div>
                            <span>{props.address}</span>
                        </div>
                    )
                }
            </div>

            {
                props.date && (
                    <div className="common-date-block opportunity-list__date">
                        {props.date}
                    </div>
                )
            }
        </Link>

    )
}
