import React from 'react';
import { Helmet } from "react-helmet";
import { NavLink } from "react-router-dom";
import { Dropdown, Modal } from 'react-bootstrap'
import cn from 'classnames';
import NewsListItem from './components/NewsListItem'
import FilterByDate from '../../Shared/FilterByDate'
import FilterByDirection from '../../Shared/FilterByDirection'

import newsService from '../../../services/news.service';

import { ReactComponent as FiltersIcon } from '../../../assets/images/filters-icon.svg';
import type { News } from "../../../types";
import ScrollBackButton from "../../Shared/ScrollBackButton";

type State = {
    newsList: Array<News>,
    hubModal: {
        isShow: boolean
    },
    isShowMore: boolean,
    isLoading: boolean
}

export default class NewsList extends React.Component<{}, State> {
    constructor(props) {
        super(props);

        this.state = {
            newsList: [],
            hubModal: {
                isShow: false
            },
            isShowMore: true,
            isLoading: false
        };
    }

    componentDidMount() {
        this.getData(`0${this.props.location.search}`);
    }

    componentDidUpdate(prevProps) {
        if(prevProps.location.search !== this.props.location.search && !this.props.location.search) {
            this.getData(`0${this.props.location.search}`);
        }
    }

    getData = (params) => {
        this.setState({ isLoading: true });
        newsService.getAllNews(params)
            .then(response => {
                this.setState({ newsList: response.data, isLoading: false })
            });
    };

    onToggleModalHub = (state) => {
        this.setState(prevState => ({
            ...prevState,
            hubModal: {
                ...prevState.hubModal,
                isShow: state === undefined ? !this.state.hubModal.isShow : state
            }
        }));
    };

    handleShowMore = () => {
        newsService.getAllNews(`${this.state.newsList.length}${this.props.location.search}`)
            .then(response => {
                if(response.data.length) {
                    this.setState(state => ({
                        newsList: [...state.newsList, ...response.data]
                    }))
                } else {
                    this.setState({ isShowMore: false })
                }

            });
    };

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Новости</title>
                </Helmet>

                <ScrollBackButton/>

                <div className="news-list-page">
                    <div className="container">
                        <div className="d-flex align-items-center justify-content-between">
                            <h1 className={'main-title'}>Новости</h1>

                            <button className={cn('mobile-filter', { 'mobile-filter--active': this.props.location.search })} onClick={() => this.onToggleModalHub()}>
                                <FiltersIcon />
                            </button>

                            <Dropdown className={"filter-dropdown"}>
                                <Dropdown.Toggle id={'newsFilter'}>
                                    <span>Применить фильтр</span>
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    <FilterByDate buttonClass={'dropdown-item'} onGetData={this.getData} />
                                    <FilterByDirection buttonClass={'dropdown-item'} onGetData={this.getData} />
                                    {this.props.location.search && <NavLink as="button" className={'dropdown-item'} activeClassName={'news-active'} to={'/news'}>Сбросить</NavLink>}
                                </Dropdown.Menu>
                            </Dropdown>
                        </div>

                        <div className="news-list">
                            {this.state.newsList.map(item => (
                                <NewsListItem {...item} key={item.id} />
                            ))}
                        </div>

                        {this.state.isShowMore && <button className={cn('show-more', { 'isLoading': this.state.isLoading })} onClick={() => this.handleShowMore()}>Показать еще</button>}
                    </div>
                </div>

                {/*---------------------------------------------------*/}

                <Modal
                    show={this.state.hubModal.isShow}
                    onHide={() => this.onToggleModalHub(false)}
                    dialogClassName="common-modal-dialog"
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Применить фильтр</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <div className="hub-modal">
                            <FilterByDate
                                buttonClass={'hub-modal__btn'}
                                onCloseHub={this.onToggleModalHub}
                                onGetData={this.getData}
                            />
                            <FilterByDirection
                                buttonClass={'hub-modal__btn'}
                                onCloseHub={this.onToggleModalHub}
                                onGetData={this.getData}
                            />
                            {this.props.location.search &&
                                <NavLink
                                    onClick={() => this.onToggleModalHub(false)}
                                    to={'/news'}
                                    className={'hub-modal__btn hub-modal__btn--reset'}
                                >Сбросить</NavLink>
                            }
                        </div>
                    </Modal.Body>
                </Modal>
            </React.Fragment>
        );
    }
}
