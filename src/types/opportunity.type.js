// @flow

export type Opportunity = {
    id: number,
    title: string,
    image: string,
    date: string,
    time: string,
    address: string
}
