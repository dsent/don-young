// @flow

export type Photo = {
    id: number,
    image: string,
    title: string,
    url: string
}
