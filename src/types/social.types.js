// @flow

export type Social = {
    vk: ?string,
    instagram: ?string,
    facebook: ?string,
    youtube: ?string
}
