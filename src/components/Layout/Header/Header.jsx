import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import { HashLink } from 'react-router-hash-link';
import cn from 'classnames';

import SearchModal from '../../Shared/SearchBlock'
import CommonSocial from "../../Shared/CommonSocial";
import FooterContacts from "../../Shared/FooterContacts";

import { ReactComponent as MenuButton } from './images/menu-button.svg'
import styles from './Header.module.scss';

import type { CommonData, ReactRef } from '../../../types'

type State = {
    mobileMenuIsShow: boolean,
    mobileContactsIsShow: boolean,
    mobileSearchRef: ReactRef<HTMLElement>,
    redirectPath: ?string
}

class Header extends React.Component<CommonData, State> {
    constructor(props) {
        super(props);

        this.state = {
            mobileMenuIsShow: false,
            mobileContactsIsShow: false,
            mobileSearchRef: React.createRef(),
        };
    }

    handleMenuToggle = (state, menu) => {
        if (menu) {
            this.setState( { [menu]: state === undefined ? state : !this.state[menu] })
        } else {
            this.setState({ mobileMenuIsShow: false, mobileContactsIsShow: false })
        }
    };

    handleSubmitForm = (e: Event) => {
        e.preventDefault();

        this.handleMenuToggle();

        const page = this.props.location.pathname.split('/');

        this.props.history.push({
            pathname: '/search',
            search: `q=${this.state.mobileSearchRef.current.value}&p=${page[1]}`
        });

        // eslint-disable-next-line react/no-direct-mutation-state
        this.state.mobileSearchRef.current.value = '';
    };

    mobileContacts = () => {
        this.setState({ mobileMenuIsShow: false, mobileContactsIsShow: true })
    };

    render() {
        return (
            <React.Fragment>
                <header className={cn(styles.block, { [styles.notMain]: this.props.location.pathname !== '/' })}>
                    <div className={styles.top}>
                        <div className="container">
                            <div className="d-flex justify-content-end">
                                <SearchModal />

                                <div className={styles.social}>
                                    <CommonSocial {...this.props.social} />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className={styles.bottom}>
                        <div className="container">
                            <div className="d-flex align-items-center justify-content-end justify-content-md-between">
                                <div className={styles.logos}>
                                    {this.props.organizers && this.props.organizers.map(item => (
                                        <a href={item.url} key={item.id} target='_blank' rel="noopener noreferrer">
                                            <img src={process.env.REACT_APP_API_BASE_URL + item.image} alt=""/>
                                        </a>
                                    ))}
                                </div>

                                <nav className={styles.menu}>
                                    <NavLink to={'/'} exact activeClassName={styles.active}>Главная</NavLink>
                                    {this.props.menu && this.props.menu.map(item => {
                                        if (item.path.indexOf('#') > -1) {
                                            return <NavLink
                                                to={item.path}
                                                key={item.id}
                                            >
                                                {item.title}
                                            </NavLink>
                                        } else
                                        if(item.path === '/direction/1/about') {
                                            return <NavLink
                                                to={item.path}
                                                key={item.id}
                                                activeClassName={styles.active}
                                                isActive={() => {
                                                    if(this.props.location.pathname.includes('direction')) {
                                                        return true;
                                                    }
                                                }}
                                            >{item.title}</NavLink>;
                                        } else {
                                            return <NavLink
                                                to={item.path}
                                                key={item.id}
                                                activeClassName={styles.active}
                                            >
                                                {item.title}
                                            </NavLink>
                                        }
                                    })}
                                </nav>

                                <button className={cn(
                                        styles.menuButton,
                                        { [styles.notMainButton]: this.props.location.pathname !== '/' }
                                    )}
                                        onClick={() => this.handleMenuToggle(true, 'mobileMenuIsShow')}
                                >
                                    <MenuButton />
                                </button>
                            </div>
                        </div>
                    </div>
                </header>

                <div className={cn('mobile-menu', { 'mobile-menu--open': this.state.mobileMenuIsShow })}>
                    <button className="mobile-menu__close" onClick={() => this.handleMenuToggle(false)} />

                    <div className="overflow-auto h-100">
                        <div className="mobile-menu__menu">
                            <NavLink
                                to={"/"}
                                onClick={() => this.handleMenuToggle(false)}
                                activeClassName='active'
                                exact
                            >
                                Главная
                            </NavLink>
                            {this.props.menu && this.props.menu.map(item => {
                                switch (item.path) {
                                    case '/#media':
                                        return <NavLink
                                            to={item.path}
                                            key={item.id}
                                            onClick={() => this.setState({ mobileMenuIsShow: false })}
                                        >
                                            {item.title}
                                        </NavLink>

                                    case '/#footer':
                                        return <HashLink
                                            to={item.path}
                                            key={item.id}
                                            onClick={() => this.mobileContacts()}
                                        >{item.title}</HashLink>;

                                    case '/direction/1/about':
                                        return <NavLink
                                            to={item.path}
                                            key={item.id}
                                            activeClassName='active'
                                            onClick={() => this.setState({ mobileMenuIsShow: false })}
                                            isActive={() => {
                                                if(this.props.location.pathname.includes('direction')) {
                                                    return true;
                                                }
                                            }}
                                        >{item.title}</NavLink>;

                                    default:
                                        return <NavLink
                                            to={item.path}
                                            key={item.id}
                                            onClick={() => this.handleMenuToggle(false)}
                                            activeClassName='active'
                                        >
                                            {item.title}
                                        </NavLink>
                                }
                            })}
                        </div>

                        <div className="mobile-menu__search">
                            <form onSubmit={this.handleSubmitForm} className={'d-flex'}>
                                <input ref={this.state.mobileSearchRef} type="text" placeholder={'Поиск'} />
                                <button type={'submit'} />
                            </form>
                        </div>

                        <div className="mobile-menu__logos">
                            {this.props.organizers && this.props.organizers.map(item => (
                                <img key={item.id} src={process.env.REACT_APP_API_BASE_URL + item.image} alt=""/>
                            ))}
                        </div>
                    </div>
                </div>

                <div className={cn('mobile-menu mobile-menu--contacts', { 'mobile-menu--open': this.state.mobileContactsIsShow })}>
                    <button className="mobile-menu__close" onClick={() => this.handleMenuToggle(false)} />

                    <div className="overflow-auto h-100">
                        <div className="mobile-menu__contacts">
                            <div className="mobile-menu__contacts-title">Контакты</div>
                            <FooterContacts {...this.props.contacts} />
                            <div className="mobile-menu__contacts-title">Для СМИ</div>
                            <FooterContacts {...this.props.massMedia} />
                            <div className="mobile-menu__contacts-title">Соцсети</div>
                            <div className="mobile-menu__social">
                                <CommonSocial {...this.props.social} />
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default withRouter(Header);
