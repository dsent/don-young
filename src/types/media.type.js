// @flow

import type { Photo } from "./photo.type";
import type { Video } from "./video.type";

export type Media = {
    photos: Array<Photo>,
    videos: Array<Video>,
    urlphotos: string,
    urlvideos: string
}
