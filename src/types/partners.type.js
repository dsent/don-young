// @flow

export type Partners = {
    id: string,
    image: string,
    url?: string,
    title?: string
}
