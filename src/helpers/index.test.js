import helpers from './index';

test('should convert object to get params', () => {
    expect(helpers.convertObject2Get({ 'from': '21.12.1990', 'to': '25.5.2012' })).toBe('from=21.12.1990&to=25.5.2012');
    expect(helpers.convertObject2Get()).toBe('');
});

test('should get search query', () => {
    expect(helpers.getSearchQuery('?q=test')).toBe('test');
    expect(helpers.getSearchQuery('?q=')).toBe('');
});
