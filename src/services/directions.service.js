import http from './http.service';

export default {
    getMainDirection() {
        return http.get(`${process.env.REACT_APP_API_BASE_URL}/api/main/trends/`)
    },

    getDirectionAbout(id) {
        return http.get(`${process.env.REACT_APP_API_BASE_URL}/api/directions/${id}`)
    }
}
