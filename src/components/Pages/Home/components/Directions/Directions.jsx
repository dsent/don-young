import React from 'react';
import { Link } from "react-router-dom";
import type { Direction } from "../../../../../types";
import directionService from '../../../../../services/directions.service';

import styles from './Directions.module.scss';

type State = {
    directions: Array<Direction>
}

export default class Directions extends React.Component<{}, State> {
    constructor(props) {
        super(props);

        this.state = {
            directions: []
        }
    }

    componentDidMount(): void {
        directionService.getMainDirection()
            .then(response => {
                this.setState({ directions: response.data });
            })
    }

    render() {
        return (
            <section className="directions-block">
                <div className="container">
                    <h2 className="common-title">направления</h2>

                    <div className={styles.list}>
                        {this.state.directions.map(item => (
                            <Link key={item.id} to={`/direction/${item.id}/about`} className={styles.item}>
                                <span>{item.title}</span>
                            </Link>
                        ))}
                    </div>
                </div>
            </section>
        )
    }
}
