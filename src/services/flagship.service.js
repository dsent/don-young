import http from './http.service';

export default {
    getMainFlagships() {
        return http.get(`${process.env.REACT_APP_API_BASE_URL}/api/main/flagmans/`)
    },

    getFlagship(filter, offset = 0) {
        return http.get(`${process.env.REACT_APP_API_BASE_URL}/api/flagships/${filter}/${offset}`)
    },
}
