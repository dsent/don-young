// @flow

export type People = {
    id: number,
    name: string,
    position: string,
    email: string,
    phone: string,
    image: string,
}
