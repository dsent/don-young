import React from 'react';
import icon from './images/more.svg';

type Props = {
    title: string,
    link: string,
    blank: boolean
}

export default function MobileMore(props: Props) {
    return (
        <a href={props.link} target={props.blank ? "_blank" : ""} rel="noopener noreferrer" className="mobile-all-albums">
            <div className="d-flex flex-column h-100 justify-content-center">
                <img src={icon} alt="" />
                <span>{props.title}</span>
            </div>
        </a>
    )
}
