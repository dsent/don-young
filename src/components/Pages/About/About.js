import React from 'react';
import { Helmet } from "react-helmet";
import { ScreenWidth } from '../../../context';
import cn from 'classnames';

import CommitteeItem from './components/CommitteeItem'
import ScrollBackButton from '../../Shared/ScrollBackButton';

import aboutService from '../../../services/about.service';

import type { About, Committee } from '../../../types';

import mailIcon from '../../../assets/images/mail-icon.svg';
import phoneIcon from '../../../assets/images/phone-icon.svg';
import addressIcon from '../../../assets/images/address-icon.svg';

export default class AboutPage extends React.Component<{}, About> {
    constructor(props) {
        super(props);

        this.state = {}
    }

    componentDidMount() {
        aboutService.getAboutData()
            .then(response => {
                let data = response.data;
                data.aboutblock_set = data.aboutblock_set.map(item => {
                    item.isOpen = false;
                    return item;
                });

                this.setState( {...data} );
            })
    }

    sidebarRender() {
        return (
            <div className="col col-sidebar">
                <div className="sidebar-sticky">
                    <h3>Режим работы</h3>

                    {this.state.work_time &&
                        <div className="sidebar-block" dangerouslySetInnerHTML={{ __html: this.state.work_time }} />
                    }

                    <h3>Общая информация</h3>

                    <div className="sidebar-block">
                        <div className="contacts">
                            {this.state.email &&
                                <div className="contacts__item">
                                    <div className="contacts__icon">
                                        <img src={mailIcon} alt=""/>
                                    </div>
                                    <a href={`mailto:${this.state.email}`}>{this.state.email}</a>
                                </div>
                            }
                            {this.state.phone &&
                                <div className="contacts__item">
                                    <div className="contacts__icon">
                                        <img src={phoneIcon} alt=""/>
                                    </div>
                                    <a href={`tel:${this.state.phone}`}>{this.state.phone}</a>
                                </div>
                            }
                            {this.state.address &&
                                <div className="contacts__item">
                                    <div className="contacts__icon">
                                        <img src={addressIcon} alt=""/>
                                    </div>
                                    <span>{this.state.address}</span>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    };

    renderCommitteeItem(blockItem: Committee) {
        let data = blockItem.isOpen ?
            blockItem.aboutblockpeople_set :
            blockItem.aboutblockpeople_set.slice(0, 2);

        return data.map(people => (
            <CommitteeItem {...people} key={people.id} />
        ))
    };

    toggleCommittee(blockItem: Committee) {
        this.setState(state => {
            const aboutblock_set = this.state.aboutblock_set.map(item => {
                if(item === blockItem) {
                    item.isOpen = !item.isOpen;
                }

                return item;
            });

            return {
                ...state,
                aboutblock_set
            }
        });
    };

    render() {
        return (
            <ScreenWidth.Consumer>
                {({screenWidth}) => (
                    <React.Fragment>
                        <Helmet>
                            <title>О нас</title>
                        </Helmet>

                        <ScrollBackButton/>

                        <div className="about-page">
                            <div className="container">
                                <h1 className={'main-title'}>О нас</h1>

                                <div className="row">
                                    {screenWidth < 1024 && this.sidebarRender()}

                                    <div className="col">
                                        {this.state.text && <div className='about-text' dangerouslySetInnerHTML={{ __html: this.state.text }} />}

                                        {this.state.aboutblock_set && this.state.aboutblock_set.map(blockItem => (
                                            <div className="committee" key={blockItem.id}>
                                                <h3 className="committee__title">{blockItem.title}</h3>

                                                <div className="committee-personal-list">
                                                    {this.renderCommitteeItem(blockItem)}
                                                </div>

                                                {
                                                    blockItem.aboutblockpeople_set.length > 2 &&
                                                    <button className={cn('committee__more', { 'committee__more--open': blockItem.isOpen })} onClick={() => this.toggleCommittee(blockItem)}>
                                                        {blockItem.isOpen ? 'Свернуть' : 'Смотреть все'}

                                                    </button>
                                                }
                                            </div>
                                        ))}
                                    </div>

                                    {screenWidth >= 1024 && this.sidebarRender()}
                                </div>
                            </div>
                        </div>
                    </React.Fragment>
                )}
            </ScreenWidth.Consumer>
        );
    }
}
