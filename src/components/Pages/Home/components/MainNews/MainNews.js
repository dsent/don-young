import React from 'react';
import Slider from "react-slick";
import { Link } from 'react-router-dom'
import helper from '../../../../../helpers';
import cn from 'classnames';

import type { News } from "../../../../../types";
import newsService from '../../../../../services/news.service';

type Props = {
    news: Array<News>
}

type State = {
    settings: any
}

export default class MainNews extends React.Component<Props, State> {
    constructor(props) {
        super(props);

        this.state = {
            settings: {
                dots: true,
                autoplay: true,
                autoplaySpeed: 5000,
                arrows: true,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1
            }
        };
    }

    componentDidMount(): void {
        newsService.getMainNews()
            .then(response => {
                this.setState({
                    newsList: response.data
                })
            })
    }

    render() {
        return(
            <Slider className="main-news" {...this.state.settings}>
                {this.props.news.map(item => (
                    <Link to={`/news/${item.id}`} className="main-news__item" key={item.id}>
                        <div className="main-news__image">
                            <img src={process.env.REACT_APP_API_BASE_URL + item.image} alt=""/>
                        </div>

                        <div className="main-news__title">
                            <span>{item.title}</span>
                        </div>

                        <div className={cn('main-news__date', 'common-date-block')}>
                            {helper.convert2LL(item.date)}
                        </div>
                    </Link>
                ))}
            </Slider>
        )
    }
}
