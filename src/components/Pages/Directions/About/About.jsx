import React from 'react';
import { Helmet } from "react-helmet";
import cn from 'classnames';

import Contacts from "./components/Contacts";
import Documents from "./components/Documents";
import Partners from "./components/Partners";

import directionsService from '../../../../services/directions.service'

import type { AboutDirection, Document } from "../../../../types";

type State = {
    data: AboutDirection,
    contentReadMore: boolean,
    docsReadMore: boolean
}

type Props = {
    title: string
}

export default class About extends React.Component<Props, State> {
    constructor(props) {
        super(props);

        this.state = {
            data: {
                trendsdocuments_set: [],
                trendscontacts_set: [],
                trendspartners_set: [],
                hrefs: []
            },
            contentReadMore: false,
            docsReadMore: false
        }
    }

    componentDidMount() {
        directionsService.getDirectionAbout(this.props.match.params.id)
            .then(response => {
                this.setState({ data: response.data })
            })
    }

    componentDidUpdate(prevProps) {
        if(prevProps.location.pathname !== this.props.location.pathname) {
            directionsService.getDirectionAbout(this.props.match.params.id)
                .then(response => {
                    this.setState({ data: response.data })
                })
        }
    }

    handleContentReadMore = () => {
        this.setState(state => ({ contentReadMore: !state.contentReadMore }));
    };

    handleDocsReadMore = () => {
        this.setState(state => ({ docsReadMore: !state.docsReadMore }));
    };

    renderDocuments = (): Array<Document> => {
        if(this.state.docsReadMore) {
            return this.state.data.trendsdocuments_set;
        } else {
            return this.state.data.trendsdocuments_set.slice(0, 4);
        }
    };

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>{`${this.props.title} - О направлении`}</title>
                </Helmet>

                {this.state.data.text &&
                    <div className="direction-page__block">
                        {
                            !this.state.contentReadMore &&
                            this.state.data.text.length > 300 ?
                            <div className={cn('direction-page__content')} dangerouslySetInnerHTML={{ __html: this.state.data.text.slice(0, 300) + '...' }} /> :
                            <div className={cn('direction-page__content')} dangerouslySetInnerHTML={{ __html: this.state.data.text }} />
                        }

                        {
                            this.state.data.text.length > 300 &&
                            (<button className={cn('direction-page__show-content', { 'direction-page__show-content--open': this.state.contentReadMore })}
                                    onClick={() => this.handleContentReadMore()}>
                                {this.state.contentReadMore ? 'Свернуть' : 'Читать далее'}
                            </button>)
                        }
                    </div>
                }

                {this.state.data.trendscontacts_set.length > 0 &&
                    <div className="direction-page__block">
                        <h3 className="direction-page__title">Контакты</h3>

                        <div className="committee-personal-list committee-personal-list--direction">
                            {this.state.data.trendscontacts_set.map(item => (
                                <Contacts {...item} key={item.id} />
                            ))}
                        </div>
                    </div>
                }

                {this.state.data.trendsdocuments_set.length > 0 &&
                    <div className="direction-page__block">
                        <h3 className="direction-page__title">Документы</h3>

                        <div className="doc-list">
                            {this.renderDocuments().map(item => (
                                <Documents key={item.id} {...item} />
                            ))}
                        </div>
                        {
                            this.state.data.trendsdocuments_set.length > 4 &&
                            (<button
                                className={cn('direction-page__show-content', { 'direction-page__show-content--open': this.state.contentReadMore })}
                                onClick={() => this.handleDocsReadMore()}
                            >
                                {this.state.docsReadMore ? 'Свернуть' : 'Показать все'}
                            </button>)
                        }
                    </div>
                }

                {this.state.data.trendscontacts_set.length > 0 &&
                    <div className="direction-page__block">
                        <h3 className="direction-page__title">Партнеры направления</h3>

                        <div className="partners-list">
                            {this.state.data.trendspartners_set.map(item => (
                                <Partners key={item.id} {...item} />
                            ))}
                        </div>
                    </div>
                }

                {this.state.data.hrefs.length > 0 &&
                    <div className="direction-page__block">
                        <h3 className="direction-page__title">Иные ссылки</h3>

                        <div className="direction-links">
                            {this.state.data.hrefs.map(item => (
                                <a href={item.url} className='direction-links__item'><span>{item.title}</span></a>
                            ))}
                        </div>
                    </div>
                }
            </React.Fragment>
        )
    }
}
