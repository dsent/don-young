// @flow

export type Contacts = {
    phone: ?string,
    email: ?string,
    address: ?string,
    map: ?string
}
