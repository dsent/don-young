import React from 'react';
import '../../../assets/styles/main.scss';
import { Switch, Route } from 'react-router-dom'

import commonService from '../../../services/common.service';
import type { CommonData } from '../../../types'

import Header from '../Header'
import Footer from '../Footer'

import Home from '../../Pages/Home';
import About from '../../Pages/About';
import NewsList from '../../Pages/NewsList';
import NewsDetail from '../../Pages/NewsDetail';
import Flagship from '../../Pages/Flagship';
import OpportunityList from '../../Pages/OpportunityList';
import OpportunityDetail from '../../Pages/OpportunityDetail';
import Territory from '../../Pages/Territory';
import Search from '../../Pages/Search';

import Directions from '../../Pages/Directions';

type State = {
    commonData: CommonData
}

export default class Main extends React.Component<{}, State> {
    constructor(props) {
        super(props);

        this.state = {
            commonData: null
        }
    }

    componentDidMount() {
        commonService.getCommonData()
            .then(response => {
                this.setState({ commonData: response.data })
            })
    }

    render() {
        return (
            <div className="wrapper">
                <Header {...this.state.commonData} />

                <div className="main">
                    <Switch>
                        <Route exact path="/" render={(props) => <Home {...props} banner={this.state.commonData && this.state.commonData.main.banner} logo={this.state.commonData && this.state.commonData.main.logo} />} />
                        <Route path="/about" component={About} />
                        <Route exact path="/news" component={NewsList} />
                        <Route path="/news/:id" component={NewsDetail} />
                        <Route path={["/flagship/:filter", "/flagship"]} component={Flagship} />
                        <Route exact path="/opportunity" component={OpportunityList} />
                        <Route exact path={"/opportunity/:filter(active|completed)"} component={OpportunityList} />
                        <Route path="/opportunity/:id" component={OpportunityDetail} />
                        <Route path={["/territory/:id", "/territory"]} component={Territory} />
                        <Route path={["/direction/:id", "/direction"]} component={Directions} />

                        <Route path={"/search"} component={Search} />
                    </Switch>
                </div>

                <Footer {...this.state.commonData} />
            </div>
        );
    }
}
