// @flow

import type { Direction } from './direction.type'

type Block = {
    id: number,
    topLine: boolean,
    bottomLine: boolean,
    title: string,
    text: string,
    newsfile: {
        file: string,
        ext: string,
        label: string,
    }
}

export type News = {
    id: number,
    title: string,
    image: string,
    date: string,
    trends: ?Array<Direction>,
    titlePreview: ?string,
    blocks: Array<Block>
}
