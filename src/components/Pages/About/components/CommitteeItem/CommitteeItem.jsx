import React from 'react';

import type { People } from '../../../../../types'

import mailIcon from '../../../../../assets/images/mail-icon.svg';
import phoneIcon from '../../../../../assets/images/phone-icon.svg';

function CommitteeItem(props: People) {
    return (
        <div className="committee-personal-list__item">
            <div className="committee-personal-list__image">
                <img src={process.env.REACT_APP_API_BASE_URL + props.image} alt=""/>
            </div>
            <div className="committee-personal-list__content">
                <div className="committee-personal-list__name">{props.name}</div>
                <div className="committee-personal-list__job">{props.position}</div>
                <div className="contacts">
                    {props.email &&
                        <div className="contacts__item contacts__item--committee-personal">
                            <div className="contacts__icon">
                                <img src={mailIcon} alt=""/>
                            </div>
                            <a href={`mailto:${props.email}`}>{props.email}</a>
                        </div>
                    }

                    {props.phone &&
                        <div className="contacts__item contacts__item--committee-personal">
                            <div className="contacts__icon">
                                <img src={phoneIcon} alt=""/>
                            </div>
                            <a href={`tel:${props.phone}`}>{props.phone}</a>
                        </div>
                    }
                </div>
            </div>
        </div>
    );
}

export default CommitteeItem;
