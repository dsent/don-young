export type Territory = {
    id: number,
    title: string
}
