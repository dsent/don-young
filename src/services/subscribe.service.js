import http from './http.service';

export default {
    sendEmail(data) {
        return http.post(`${process.env.REACT_APP_API_BASE_URL}/api/subs/`, data);
    }
}
