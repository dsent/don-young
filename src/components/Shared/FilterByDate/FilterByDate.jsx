import React  from 'react';
import { withRouter } from 'react-router-dom'
import { Modal } from "react-bootstrap";

// import DayPicker, { DateUtils } from "react-day-picker";
// import MomentLocaleUtils from 'react-day-picker/moment';
import 'react-dates/initialize';
import { DayPickerRangeController } from 'react-dates';

import helper from "../../../helpers";
import moment from "moment";
import _ from "lodash";
import { ReactRef } from '../../../types'

type Props = {
    buttonClass: string,
    onCloseHub?: (?boolean) => void,
    onGetData: (?{ from: string, to: string }) => void
}

type State = {
    isShow: boolean,
    desktop: {
        from: string,
        to: string,
    },
    focusedInput: string,
    mobile: {
        from: {
            day: ReactRef<HTMLElement>,
            month: ReactRef<HTMLElement>,
            year: ReactRef<HTMLElement>,
        },
        to: {
            day: ReactRef<HTMLElement>,
            month: ReactRef<HTMLElement>,
            year: ReactRef<HTMLElement>,
        }
    }
}

class FilterByDate extends React.Component<Props, State> {
    constructor(props) {
        super(props);

        this.state = {
            isShow: false,
            desktop: {
                from: null,
                to: null
            },
            focusedInput: 'startDate',
            mobile: {
                from: {
                    day: React.createRef(),
                    month: React.createRef(),
                    year: React.createRef(),
                },
                to: {
                    day: React.createRef(),
                    month: React.createRef(),
                    year: React.createRef(),
                }
            }
        };

        this.onDatesChange = this.onDatesChange.bind(this);
        this.onFocusChange = this.onFocusChange.bind(this);
    }

    onToggleModal = (state) => {
        if (!state && this.props.onCloseHub) {
            this.props.onCloseHub(false);
        }

        this.setState({ isShow: state === undefined ? !this.state.isShow : state });
    };

    selectDate = (from, to) => {
        if(!to) to = from;

        this.props.history.push({
            pathname: this.props.location.pathname,
            search: `from=${from}&to=${to}`
        });

        this.props.onGetData(`0${helper.convertObject2Get({from, to})}`, true);

        this.onToggleModal(false);
    };

    // handleDayClick = (day) => {
    //     const range = DateUtils.addDayToRange(day, this.state.desktop);
    //
    //     this.setState(prevState => ({
    //         ...prevState,
    //         desktop: range
    //     }));
    // };

    resetDatePicker = () => {
        this.setState(prevState => ({
            ...prevState,
            desktop: this.datePickerInitialState()
        }));
    };

    datePickerInitialState = () => {
        return {
            from: null,
            to: null
        }
    };

    onDatesChange = ({ startDate, endDate }) => {
        this.setState(prevState => ({
            ...prevState,
            desktop: { from: startDate, to: endDate }
        }));
    };

    onFocusChange = (focusedInput) => {
        this.setState({
            focusedInput: !focusedInput ? 'startDate' : focusedInput,
        });
    };

    render() {
        return (
            <React.Fragment>
                <button className={this.props.buttonClass} onClick={() => this.onToggleModal(true)}>По дате</button>

                <Modal
                    show={this.state.isShow}
                    onHide={() => this.onToggleModal(false)}
                    dialogClassName="common-modal-dialog"
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Выберите дату</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <div className="date-range">
                            <div className="date-range__item">
                                <DayPickerRangeController
                                    noBorder
                                    hideKeyboardShortcutsPanel
                                    numberOfMonths={2}
                                    focusedInput={this.state.focusedInput}
                                    daySize={50}
                                    onDatesChange={this.onDatesChange}
                                    onFocusChange={this.onFocusChange}
                                    startDate={this.state.desktop.from}
                                    endDate={this.state.desktop.to} />
                            </div>
                            <div className="date-range__item">
                                <div className="date-range__title">Сейчас выбрано:</div>
                                <div className="date-range__output">
                                    { this.state.desktop.from && helper.convert2DatePicker(this.state.desktop.from) }
                                    { this.state.desktop.to && ` - ${helper.convert2DatePicker(this.state.desktop.to)}` }
                                </div>

                                <button
                                    className={'date-range__reset'}
                                    disabled={!this.state.desktop.from}
                                    onClick={() => this.resetDatePicker()}
                                >Сбросить</button>
                                <button
                                    className={'date-range__apply'}
                                    disabled={!this.state.desktop.from && !this.state.desktop.to}
                                    onClick={() => this.selectDate(
                                        helper.convert2DatePicker(this.state.desktop.from),
                                        helper.convert2DatePicker(this.state.desktop.to)
                                    )}
                                >Применить</button>
                            </div>
                        </div>

                        <div className="date-range-mobile">
                            <div className="date-range-mobile__title">начало:</div>

                            <div className="date-range-mobile__date">
                                <select ref={this.state.mobile.from.month}>
                                    {moment.months().map((item, index) => (
                                        <option value={index+1} key={item}>{item}</option>
                                    ))}
                                </select>

                                <select ref={this.state.mobile.from.day}>
                                    {_.times(31).map(item => (
                                        <option value={item+1} key={item}>{item+1}</option>
                                    ))}
                                </select>

                                <select ref={this.state.mobile.from.year}>
                                    {_.rangeRight(2015, moment().year()+1).map(item => (
                                        <option value={item} key={item}>{item}</option>
                                    ))}
                                </select>
                            </div>

                            <div className="date-range-mobile__title">окончание:</div>

                            <div className="date-range-mobile__date">
                                <select ref={this.state.mobile.to.month} defaultValue={moment().month()+1}>
                                    {moment.months().map((item, index) => (
                                        <option value={index+1} key={item}>{item}</option>
                                    ))}
                                </select>

                                <select ref={this.state.mobile.to.day} defaultValue={moment().date()}>
                                    {_.times(31).map(item => (
                                        <option value={item+1} key={item}>{item+1}</option>
                                    ))}
                                </select>

                                <select ref={this.state.mobile.to.year}>
                                    {_.rangeRight(2015, moment().year()+1).map(item => (
                                        <option value={item} key={item}>{item}</option>
                                    ))}
                                </select>
                            </div>

                            <button
                                className={'hub-modal__btn'}
                                onClick={() => this.selectDate(
                                    `${this.state.mobile.from.day.current.value}.${this.state.mobile.from.month.current.value}.${this.state.mobile.from.year.current.value}`,
                                    `${this.state.mobile.to.day.current.value}.${this.state.mobile.to.month.current.value}.${this.state.mobile.to.year.current.value}`
                                )}
                            >Применить</button>
                        </div>
                    </Modal.Body>
                </Modal>
            </React.Fragment>
        );
    }
}

export default withRouter(FilterByDate);
