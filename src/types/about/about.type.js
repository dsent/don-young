// @flow
import type { Committee } from './committee.type'

export type About = {
    work_time: string,
    email: string,
    phone: string,
    address: string,
    aboutblock_set: Array<Committee>
}
