// @flow

export type Flagship = {
    id: number,
    title: string,
    date: string,
    image: string,
    address: string,
    url: string
}
