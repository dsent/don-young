import React from 'react';
import { Helmet } from "react-helmet";
import { NavLink, Redirect } from 'react-router-dom';
import cn from 'classnames'

import FlagshipItem from '../../Shared/FlagshipItem';

import flagshipService from '../../../services/flagship.service'

import type { Flagship } from '../../../types'
import ScrollBackButton from "../../Shared/ScrollBackButton";

type State = {
    flagship: Array<Flagship>,
    isShowMore: boolean,
    isLoading: boolean
}

export default class Flagships extends React.Component<{}, State> {
    constructor(props) {
        super(props);

        this.state = {
            flagship: [],
            isShowMore: true,
            isLoading: false
        }
    }

    componentDidMount(): void {
        if(this.props.match.params.filter) {
            this.getData(this.props.match.params.filter, '0', 'replace');
        }
    }

    componentDidUpdate(prevProps) {
        if(prevProps.location.pathname !== this.props.location.pathname) {
            this.setState({ isShowMore: true });
            this.getData(this.props.match.params.filter, '0', 'replace');
        }
    }

    getData = (filter, offset, action) => {
        this.setState({ isLoading: true });
        flagshipService.getFlagship(filter, offset)
            .then(response => {
                if(action === 'add') {
                    if(response.data.length) {
                        this.setState(state => ({
                            ...state.flagship,
                            isLoading: false,
                            ...response.data
                        }))
                    } else {
                        this.setState({ isShowMore: false, isLoading: false });
                    }
                } else if(action === 'replace') {
                    if(response.data.length) {
                        this.setState({ flagship: response.data, isLoading: false })
                    } else {
                        this.setState({ flagship: response.data, isLoading: false, isShowMore: false });
                    }
                }
            })
    };

    handleShowMore = () => {
        this.getData(this.props.match.params.filter, this.state.flagship.length, 'add');
    };

    redirectOnCurrent = () => {
        if(!this.props.match.params.filter) {
            return <Redirect to={'/flagship/active'} />
        } else {
            return null;
        }
    };

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Флагманы</title>
                </Helmet>

                <ScrollBackButton/>

                {this.redirectOnCurrent()}

                <div className="flagship-page">
                    <div className="container">
                        <div className="d-lg-flex align-items-center justify-content-between">
                            <h1 className={'main-title'}>Флагманы</h1>

                            <div className="actual-realize-buttons actual-realize-buttons--flagship">
                                <NavLink
                                    to={'/flagship/active'}
                                    className={cn({'active': this.props.match.params.filter === 'active'})}
                                >
                                    <span>актуальные</span>
                                </NavLink>
                                <NavLink
                                    to={'/flagship/completed'}
                                    className={cn({'active': this.props.match.params.filter === 'completed'})}
                                >
                                    <span>реализованные</span>
                                </NavLink>
                            </div>
                        </div>

                        <div className="row flagship-list">
                            {this.state.flagship.map(item => (
                                <div className="col-xl-3 col-lg-4 col-md-6 col-12 mb-md-3" key={item.id}>
                                    <FlagshipItem {...item} />
                                </div>
                            ))}
                        </div>

                        {this.state.isShowMore && <button className={cn('show-more', { 'isLoading': this.state.isLoading })} onClick={() => this.handleShowMore()}>Показать еще</button>}

                    </div>
                </div>
            </React.Fragment>

        );
    }
}
