// @flow

import React from 'react';
import VanillaToasts from 'vanillatoasts';
import styles from './FooterSubscribe.module.scss';
import type { ReactRef } from '../../../types';

import subscribeService from '../../../services/subscribe.service';

export default class FooterSubscribe extends React.Component<{}> {
    input: ReactRef<HTMLInputElement> = React.createRef();

    sendSubscribe = () => {
        if(this.validateEmail(this.input.current.value)) {
            subscribeService.sendEmail({ email: this.input.current.value })
                .then(response => {
                    this.input.current.value = '';
                    VanillaToasts.create({
                        title: 'Спасибо за подписку',
                        type: 'success',
                        timeout: 5000
                    })
                })
                .catch(error => {
                    VanillaToasts.create({
                        title: error.response.data.email[0],
                        type: 'error',
                        timeout: 5000
                    })
                })
        } else {
            VanillaToasts.create({
                title: 'Введите корректный адрес электронной почты.',
                type: 'error',
                timeout: 5000
            })
        }

    };

    validateEmail = (email) => {
        // eslint-disable-next-line
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    };

    render() {
        return (
            <React.Fragment>
                <div className={styles.block}>
                    <input type="text" placeholder="введите ваш почтовый ящик" ref={this.input} />
                    <button onClick={() => this.sendSubscribe()}>Подписаться</button>
                </div>
            </React.Fragment>
        )
    }
}
