import http from './http.service';

export default {
    getSearchResult(page, query) {
        return http.get(`${process.env.REACT_APP_API_BASE_URL}/api/search/${page}?${encodeURI(query)}`);
    }
}
