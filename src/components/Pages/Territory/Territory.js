import React from 'react';
import { Helmet } from "react-helmet";
import { Redirect } from "react-router-dom";
import cn from 'classnames';

import { Modal } from 'react-bootstrap';

import CommitteeItem from "../About/components/CommitteeItem";

import type { Territory } from '../../../types'
import territoryService from '../../../services/territory.service'

import committeeStatisticsIconMan from '../../../assets/images/committee-statistics__icon-1.svg'
import committeeStatisticsIconChart from '../../../assets/images/committee-statistics__icon-2.svg'
import mailIcon from "../../../assets/images/mail-icon.svg";
import phoneIcon from "../../../assets/images/phone-icon.svg";
import addressIcon from "../../../assets/images/address-icon.svg";
import siteIcon from "../../../assets/images/site-icon.svg";

import vk from "../../../assets/images/territory-vk.png";
import fb from "../../../assets/images/territory-fb.png";
import insta from "../../../assets/images/territory-insta.png";
import ScrollBackButton from "../../Shared/ScrollBackButton";

type State = {
    data: any,
    territories: {
        cities: Array<Territory>,
        districts: Array<Territory>,
    },
    modal: {
        isShow: boolean
    }
}

export default class Territories extends React.Component<{}, State> {
    constructor(props) {
        super(props);

        this.state = {
            data: null,
            territories: {
                cities: [],
                districts: [],
            },
            modal: {
                isShow: false
            }
        }
    }

    componentDidMount(): void {
        territoryService.getTerritoryList()
            .then(response => {
                this.setState({ territories: response.data });
            });

        if(this.props.match.params.id) {
            territoryService.getTerritoryInfo(this.props.match.params.id)
                .then(response => {
                    this.setState({ data: response.data })
                });
        }
    }

    componentDidUpdate(prevProps) {
        if(prevProps.location.pathname !== this.props.location.pathname) {
            territoryService.getTerritoryInfo(this.props.match.params.id)
                .then(response => {
                    this.setState({ data: response.data })
                });
        }
    }

    onToggleModal = (state) => {
        this.setState(prevState => ({
            ...prevState,
            modal: {
                ...prevState.modal,
                isShow: state === undefined ? !this.state.modal.isShow : state
            }
        }));
    };

    changeRegion = (id) => {
        this.setState(prevState => ({
            ...prevState,
            modal: {
                ...prevState.modal,
                isShow: false
            }
        }));

        this.props.history.push({
            pathname: `/territory/${id}`,
        })
    };

    redirectToRostov = () => {
        if(!this.props.match.params.id && this.state.territories.cities.length > 0) {
            return <Redirect to={`/territory/${this.state.territories.cities[0].id}`}/>
        } else {
            return null;
        }
    };

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Территории Ростовской области</title>
                </Helmet>

                <ScrollBackButton isMoreOffset={true}/>

                {this.redirectToRostov()}

                <div className="territory-page">
                    <div className="mobile-directions">
                        <button className="mobile-directions__button" onClick={() => this.onToggleModal()}>Выберите город или район РО</button>
                    </div>

                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <h1 className={'main-title'}>Территории Ростовской области</h1>

                                {this.state.data &&
                                    <div className="territory-info">
                                        <div className="territory-info__container">
                                            <h3 className="territory-info__title">{this.state.data.title}</h3>

                                            {this.state.data.agency &&
                                                <div className="territory-info__block">
                                                    <div className="territory-info__logo">
                                                        <img
                                                            src={process.env.REACT_APP_API_BASE_URL + this.state.data.agency.image}
                                                            alt=""/>
                                                    </div>
                                                    <div className="territory-info__content">
                                                        <div
                                                            className="territory-info__committee-name">{this.state.data.agency.title}</div>
                                                        <div className='mb-3' dangerouslySetInnerHTML={{ __html: this.state.data.agency.text }} />

                                                        <div className="committee-statistics">
                                                            {this.state.data.countYouth &&
                                                            <div className="committee-statistics__item">
                                                                <div className="committee-statistics__icon">
                                                                    <img src={committeeStatisticsIconMan} alt=""/>
                                                                </div>
                                                                <span><b>{this.state.data.countYouth}</b> молодежи в регионе</span>
                                                            </div>
                                                            }

                                                            {this.state.data.percentageYouth &&
                                                            <div className="committee-statistics__item">
                                                                <div className="committee-statistics__icon">
                                                                    <img src={committeeStatisticsIconChart} alt=""/>
                                                                </div>
                                                                <span><b>{this.state.data.percentageYouth}</b> молодежи от числа жителей региона</span>
                                                            </div>
                                                            }
                                                        </div>
                                                    </div>
                                                </div>
                                            }

                                            {!this.state.data.agency &&
                                                <div className="committee-statistics">
                                                    {this.state.data.countYouth &&
                                                    <div className="committee-statistics__item">
                                                        <div className="committee-statistics__icon">
                                                            <img src={committeeStatisticsIconMan} alt=""/>
                                                        </div>
                                                        <span><b>{this.state.data.countYouth}</b> молодежи в регионе</span>
                                                    </div>
                                                    }

                                                    {this.state.data.percentageYouth &&
                                                    <div className="committee-statistics__item">
                                                        <div className="committee-statistics__icon">
                                                            <img src={committeeStatisticsIconChart} alt=""/>
                                                        </div>
                                                        <span><b>{this.state.data.percentageYouth}</b> молодежи от числа жителей региона</span>
                                                    </div>
                                                    }
                                                </div>
                                            }
                                        </div>

                                        {this.state.data.districtcontacts_set.length > 0 &&
                                            <div className="territory-info__container">
                                                {this.state.data.districtcontacts_set.map(department => (
                                                    <React.Fragment key={department.title}>
                                                        <h3 className="territory-info__title">{department.title}</h3>
                                                        <div className="committee-personal-list">
                                                            {department.contacts.map(people => (
                                                                <CommitteeItem {...people} key={people.id}/>
                                                            ))}
                                                        </div>
                                                    </React.Fragment>
                                                ))}
                                            </div>
                                        }
                                    </div>
                                }
                            </div>
                            <div className="col col-sidebar">
                                <div className="sidebar-sticky">
                                    <button className={'territory-select'} onClick={() => this.onToggleModal()}>
                                        <span>Выберите город или район РО</span>
                                    </button>

                                    <h3>Общая информация</h3>

                                    {this.state.data &&
                                        <div className="sidebar-block">
                                            <div className="contacts">
                                                {this.state.data.url &&
                                                    <div className="contacts__item">
                                                        <div className="contacts__icon">
                                                            <img src={siteIcon} alt=""/>
                                                        </div>
                                                        <a href={`//${this.state.data.url}`} target='_blank' rel="noopener noreferrer">{this.state.data.url}</a>
                                                    </div>
                                                }
                                                {this.state.data.email &&
                                                    <div className="contacts__item">
                                                        <div className="contacts__icon">
                                                            <img src={mailIcon} alt=""/>
                                                        </div>
                                                        <a href={`mailto:${this.state.data.email}`}>{this.state.data.email}</a>
                                                    </div>
                                                }
                                                {this.state.data.phone &&
                                                    <div className="contacts__item">
                                                        <div className="contacts__icon">
                                                            <img src={phoneIcon} alt=""/>
                                                        </div>
                                                        <a href={`tel:${this.state.data.phone}`}>{this.state.data.phone}</a>
                                                    </div>
                                                }
                                                {this.state.data.address &&
                                                    <div className="contacts__item">
                                                        <div className="contacts__icon">
                                                            <img src={addressIcon} alt=""/>
                                                        </div>
                                                        <a href={this.state.data.map} target='_blank' rel="noopener noreferrer">{this.state.data.address}</a>
                                                    </div>
                                                }

                                                <div className="contacts__item mt-5">
                                                    {this.state.data.vk &&
                                                        <a href={this.state.data.vk} className='mr-3' target='_blank' rel="noopener noreferrer">
                                                            <img src={vk} alt=""/>
                                                        </a>
                                                    }

                                                    {this.state.data.facebook &&
                                                        <a href={this.state.data.facebook} className='mr-3' target='_blank' rel="noopener noreferrer">
                                                            <img src={fb} alt=""/>
                                                        </a>
                                                    }

                                                    {this.state.data.instagram &&
                                                        <a href={this.state.data.instagram} target='_blank' rel="noopener noreferrer">
                                                            <img src={insta} alt=""/>
                                                        </a>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <Modal
                    show={this.state.modal.isShow}
                    onHide={() => this.onToggleModal(false)}
                    dialogClassName="common-modal-dialog"
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Выберите город или район РО</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <div className="overflow-auto">
                            <div className="direction-list direction-list--territory">
                                <div className="row no-gutters">
                                    <div className="col-3">
                                        {this.state.territories.cities.map(item => (
                                            <button key={item.id} className={cn({ 'active': item.id.toString() === this.props.match.params.id})} onClick={() => this.changeRegion(item.id)}>
                                                {item.title}
                                            </button>
                                        ))}
                                    </div>
                                    <div className="col-9">
                                        <div className="row no-gutters">
                                            {this.state.territories.districts.map(item => (
                                                <div className="col-4" key={item.id}>
                                                    <button className={cn('region', { 'active': item.id.toString() === this.props.match.params.id})} onClick={() => this.changeRegion(item.id)}>
                                                        {item.title}
                                                    </button>
                                                </div>
                                            ))}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </React.Fragment>
        );
    }
}
