import React from 'react';
import { Link } from 'react-router-dom';

import FooterContacts from '../../Shared/FooterContacts';
import FooterSubscribe from '../../Shared/FooterSubscribe';
import CommonSocial from '../../Shared/CommonSocial'

import type { CommonData } from '../../../types'

import styles from './Footer.module.scss';

function Footer(props: CommonData) {
    return (
        <footer id="footer" className={"footer " + styles.block}>
            <div className={styles.subscribe}>
                <div className={styles.title}>Подпишись на молодежные новости и возможности</div>
                <FooterSubscribe/>
            </div>

            <div className="container">
                <div className="row">
                    <div className="col-xl-6 col-4 d-none d-sm-block">
                        <div className="row">
                            <div className="col-xl-6">
                                <div className={styles.title}>Контакты</div>
                                <FooterContacts {...props.contacts} />
                            </div>
                            <div className="col-xl-6">
                                <div className={styles.title}>Для СМИ</div>
                                <FooterContacts {...props.massMedia} />
                            </div>
                        </div>

                        <div className={styles.subscribeDesktop + " d-none d-xl-block"}>
                            <div className={styles.title}>Подпишись на молодежные новости и возможности</div>
                            <FooterSubscribe/>
                        </div>
                    </div>
                    <div className="col-xl-3 col-4 d-none d-sm-block">
                        <div className={styles.title}>Навигация</div>

                        <nav className={styles.linkList}>
                            {props.menu && props.menu.map(item => (
                                <Link to={item.path} key={item.id}>{item.title}</Link>
                            ))}
                        </nav>
                    </div>
                    <div className="col-xl-3 col-4 d-none d-sm-block">
                        <div className={styles.title}>Партнеры</div>
                        
                        <div className={styles.partners}>
                            {props.partners && props.partners.map(item => (
                                <a href={item.url} key={item.id}>
                                    <img src={process.env.REACT_APP_API_BASE_URL + item.image} alt=""/>
                                </a>
                            ))}
                        </div>

                        <div className={styles.title}>Соцсети</div>

                        <div className={styles.social}>
                            <CommonSocial {...props.social} />
                        </div>
                    </div>
                    <div className="col-12">
                        <div className={styles.copyright}>
                            {props.main && props.main.copyright}
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    );
}

export default Footer;
