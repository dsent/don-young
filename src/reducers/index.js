import { combineReducers } from 'redux';

import commonData from './commonData';

export default combineReducers({
    commonData
});
