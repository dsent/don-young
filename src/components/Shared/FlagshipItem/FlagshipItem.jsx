import React from 'react';
import cn from 'classnames';

import type { Flagship } from '../../../types';
import style from './FlagshipItem.module.scss';
import mapPoint from './images/map-point.svg';

export default function FlagshipItem(props: Flagship) {
    return (
        <a href={props.url} target='_blank' rel="noopener noreferrer" className={style.block}>
            <div className={style.image}>
                <img src={process.env.REACT_APP_API_BASE_URL + props.image} alt=""/>
            </div>
            <div className={style.title}>{props.title}</div>
            <div className={style.address}>
                <img src={mapPoint} alt=""/>
                <span>{props.address}</span>
            </div>
            <div className={cn(style.date, 'common-date-block')}>{props.date}</div>
        </a>
    );
}
