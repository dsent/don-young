import React from 'react';
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import cn from "classnames";

import searchService from '../../../services/search.service';

import helpers from '../../../helpers';

import type { ReactRef } from "../../../types";

import {ReactComponent as ButtonIcon} from "../../../assets/images/search-page-button.svg";

type State = {
    query: string,
    prevPage: string,
    input: ReactRef<HTMLInputElement>,
    count: number,
    isShowMore: boolean,
    isLoading: boolean,
    results: Array<any>,
    categories: any
}

export default class Search extends React.Component<{}, State> {
    constructor(props) {
        super(props);

        this.state = {
            query: helpers.getSearchQuery(props.location.search, 'q'),
            prevPage: helpers.getSearchQuery(props.location.search, 'p'),
            input: React.createRef(),
            count: 0,
            isShowMore: true,
            isLoading: false,
            results: [],
            categories: {
                news: 'Новости',
            }
        }
    }

    componentDidMount(): void {
        if(this.state.query) {
            searchService.getSearchResult(0, `q=${this.state.input.current.value}&p=${this.state.prevPage}`)
                .then(response => {
                    this.setState({ count: response.data.count, results: response.data.data })
                })
        }
    }

    handleSubmitForm = (e: Event) => {
        e.preventDefault();

        searchService.getSearchResult(0, `q=${this.state.input.current.value}&p=${this.state.prevPage}`)
            .then(response => {
                this.setState({ query: this.state.input.current.value, count: response.data.count, results: response.data.data })
            });

        this.props.history.push({
            pathname: '/search',
            search: `q=${this.state.input.current.value}`
        });
    };

    handleShowMore = () => {
        searchService.getSearchResult(this.state.results.length, `q=${this.state.query}&p=${this.state.prevPage}`)
            .then(response => {
                if(response.data.data.length) {
                    this.setState(state => ({
                        results: [...state.results, ...response.data.data]
                    }))
                } else {
                    this.setState({ isShowMore: false })
                }

            });
    };

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Результаты поиска {this.state.query}</title>
                </Helmet>

                <div className="search-page">
                    <div className="container">
                        <h1 className="main-title">Результаты поиска</h1>

                        <div className="search-form">
                            <form className={'d-flex'} onSubmit={this.handleSubmitForm}>
                                <input type="text" ref={this.state.input} defaultValue={this.state.query} placeholder="Текст поискового запроса" />
                                <button type={'submit'}>
                                    <ButtonIcon />
                                </button>
                            </form>
                        </div>

                        <div className="search-title">
                            По запросу {this.state.query && <b>{this.state.query}</b>} найдено: <b>{this.state.count}</b>
                        </div>

                        <div className="search-result">
                            {this.state.results.map(item => (
                                <Link to={`/${item.type}/${item.id}`} className="search-result__item" key={item.id}>
                                    <div className="search-result__type">{this.state.categories[item.type]}</div>
                                    <div className="search-result__title">{item.title}</div>
                                    <p>{item.titlePreview}</p>
                                </Link>
                            ))}
                        </div>

                        {this.state.isShowMore && <button className={cn('show-more', 'mt-5', { 'isLoading': this.state.isLoading })} onClick={() => this.handleShowMore()}>Показать еще</button>}
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
