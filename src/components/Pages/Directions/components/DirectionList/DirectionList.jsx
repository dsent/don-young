import React from 'react';
import { NavLink, withRouter } from "react-router-dom";

import type { Direction } from "../../../../../types";

import styles from './DirectionList.module.scss'

type Props = {
    list: Array<Direction>
}

class DirectionList extends React.Component<Props> {
    state = {
        stickyOffset: 0
    };

    componentDidMount() {
        const windowHeight = document.documentElement.clientHeight;
        const intElemOffsetHeight = document.getElementById('stickySidebar').offsetHeight;

        this.setState({ stickyOffset: windowHeight-intElemOffsetHeight })
    }

    render() {
        return (
            <div id="stickySidebar" className={styles.block} style={{ top: this.state.stickyOffset }}>
                {this.props.list.map(item => (
                    <NavLink
                        to={`/direction/${item.id}/about`}
                        activeClassName={styles.active}
                        isActive={() => {
                            if(this.props.match.url === `/direction/${item.id}`) {
                                return true;
                            }
                        }}
                        className={styles.item} key={item.id}
                    >
                        <div className={styles.icon}>
                            <img src={process.env.REACT_APP_API_BASE_URL + item.imageInactive} alt=""/>
                            <img src={process.env.REACT_APP_API_BASE_URL + item.imageActive} alt=""/>
                        </div>

                        <span>{item.title}</span>
                    </NavLink>
                ))}
            </div>
        )
    }
}

export default withRouter(DirectionList);
