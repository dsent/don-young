// @flow

import * as React from 'react';
import { Helmet } from "react-helmet";
import { goToAnchor } from 'react-scrollable-anchor';

import Banner from "./components/Banner";
import NewsBlock from "./components/NewsBlock";
import FlagshipBlock from "./components/FlagshipBlock";
import OpportunityBlock from "./components/OpportunityBlock";
import Directions from "./components/Directions";
import MediaBlock from "./components/MediaBlock";
import TerritoryBlock from "./components/TerritoryBlock";

export default class Home extends React.Component<{}> {
    handleGoToAnchor = () => {
        if(this.props.location.hash === '#media') {
            setTimeout(() => goToAnchor('media'), 0);
        }

        if(this.props.location.hash === '#footer') {
            setTimeout(() => goToAnchor('footer'), 0);
        }
    };

    componentDidUpdate(prevProps) {
        if(prevProps.location.hash !== this.props.location.hash) {
            if(this.props.location.hash === '#media') {
                setTimeout(() => goToAnchor('media'), 0);
            }

            if(this.props.location.hash === '#footer') {
                setTimeout(() => goToAnchor('footer'), 0);
            }
        }
    }

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Главная</title>
                </Helmet>

                <div className="home-wrap">
                    <Banner banner={this.props.banner} logo={this.props.logo}/>
                    <NewsBlock />
                    <FlagshipBlock />
                    <OpportunityBlock/>
                    <Directions/>
                    <MediaBlock loadFlag={this.handleGoToAnchor} />
                    <TerritoryBlock/>
                </div>
            </React.Fragment>
        );
    }
}
