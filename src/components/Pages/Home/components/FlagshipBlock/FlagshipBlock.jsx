import React from 'react';
import { Link } from "react-router-dom";
import Slider from 'react-slick';

import FlagshipItem from '../../../../Shared/FlagshipItem';

import type { Flagship } from "../../../../../types";
import flagshipService from '../../../../../services/flagship.service';

type State = {
    flagships: Array<Flagship>,
    settings: any
}

export default class FlagshipBlock extends React.Component<{}, State> {
    constructor(props) {
        super(props);

        this.state = {
            flagships: [],
            settings: {
                dots: true,
                arrows: false,
                infinite: false,
                slidesToShow: 4,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 1280,
                        settings: {
                            slidesToShow: 3,
                        }
                    },
                    {
                        breakpoint: 1010,
                        settings: {
                            slidesToShow: 2,
                        }
                    },
                    {
                        breakpoint: 550,
                        settings: {
                            slidesToShow: 1,
                        }
                    }
                ]
            }
        }
    }

    componentDidMount(): void {
        flagshipService.getMainFlagships()
            .then(response => {
                this.setState({ flagships: response.data });
            })
    }

    render() {
        return (
            <section className="flagship-block">
                <div className="container">
                    <div className="d-md-flex justify-content-between align-items-end">
                        <h2 className="common-title">флагманы</h2>
                        <div className="actual-realize-buttons mb-4">
                            <Link className={'active'} to={"/flagship/active"}>
                                <span>актуальные</span>
                            </Link>
                            <Link to={"/flagship/completed"}>
                                <span>реализованные</span>
                            </Link>
                        </div>
                    </div>

                    <Slider className="flagship-carousel slider-dotted-line" {...this.state.settings}>
                        {this.state.flagships.map(item => (
                            <div className={'flagship-carousel__item'} key={item.id}>
                                <FlagshipItem {...item} />
                            </div>
                        ))}
                    </Slider>
                </div>
            </section>
        )
    }
}
