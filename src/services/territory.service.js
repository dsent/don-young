import http from './http.service';

export default {
    getTerritoryList() {
        return http.get(`${process.env.REACT_APP_API_BASE_URL}/api/main/districts/`)
    },

    getTerritoryInfo(id) {
        return http.get(`${process.env.REACT_APP_API_BASE_URL}/api/districts/${id}`)
    },
}
