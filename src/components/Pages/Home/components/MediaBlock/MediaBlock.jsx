import React from 'react';
import mediaService from '../../../../../services/media.service';
import { Media } from '../../../../../types';
import Slider from 'react-slick';
import MagnificPopup from '../../../../Shared/MagnificPopup';
import helper from '../../../../../helpers';
import MobileMore from '../../../../Shared/MobileMore';
import { ScreenWidth } from '../../../../../context'

type State = Media & {
    videoSettings: any,
    photoSettings: any,
    popup: {
        isShow: boolean,
        videoId: string | null,
        videoTitle: string
    }
}

type Props = {
    loadFlag: () => void
}

export default class MediaBlock extends React.Component<Props, State> {
    constructor(props) {
        super(props);

        this.state = {
            photos: [],
            videos: [],
            videoSettings: {
                dots: true,
                autoplay: true,
                autoplaySpeed: 5000,
                arrows: true,
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 650,
                        settings: {
                            arrows: false,
                            autoplay: false
                        }
                    }
                ]
            },
            photoSettings: {
                dots: true,
                arrows: false,
                infinite: false,
                slidesToShow: 3,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 1010,
                        settings: {
                            slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 550,
                        settings: {
                            slidesToShow: 1
                        }
                    }
                ]
            },
            popup: {
                isShow: false,
                videoId: null,
                videoTitle: ''
            }
        }
    }

    componentDidMount(): void {
        mediaService.getAllMedia()
            .then(response => {
                this.setState(response.data);
                this.props.loadFlag();
            })
    }

    openVideoPopup = (videoId: string, videoTitle: string) => {
        this.setState({ popup: { isShow: true, videoId: videoId, videoTitle: videoTitle } });
    };

    closePopup = () => {
        this.setState({ popup: { isShow: false, videoId: null, videoTitle: '' } });
    };

    render() {
        return (
            <ScreenWidth.Consumer>
                {({screenWidth}) => (
                    <section id="media" className="media-block">
                        <div className="container">
                            <h2 className="common-title">Медиа</h2>

                            <Slider {...this.state.photoSettings} className="media-mobile-carousel d-xl-none">
                                {this.state.photos.map(item => (
                                    <a href={item.url} key={item.id} target="_blank" rel="noopener noreferrer" className="photo-item">
                                        <img src={process.env.REACT_APP_API_BASE_URL + item.image} alt=""/>
                                        <span className="photo-item__title">{item.title}</span>
                                    </a>
                                ))}

                                <MobileMore blank={true} link={this.state.urlphotos} title={'все фото'} />
                            </Slider>

                            <div className="row">
                                <div className="col-xl-6 d-none d-xl-block">
                                    <div className="row">
                                        {this.state.photos.map(item => (
                                            <div className="col-6" key={item.id}>
                                                <a href={item.url} target="_blank" rel="noopener noreferrer" className="photo-item">
                                                    <img src={process.env.REACT_APP_API_BASE_URL + item.image} alt=""/>
                                                    <span className="photo-item__title">{item.title}</span>
                                                </a>
                                            </div>
                                        ))}
                                    </div>

                                    <div className="text-right pt-4">
                                        <a href={this.state.urlphotos} target="_blank" rel="noopener noreferrer" className="common-button">все фото</a>
                                    </div>
                                </div>
                                <div className="col-xl-6">
                                    <Slider className="video-block" {...this.state.videoSettings}>
                                        {this.state.videos.map(item => (
                                            <div className="video-item" key={item.id} onClick={() => this.openVideoPopup(helper.getVideoId(item.url), item.title)}>
                                                <div className="video-item__image">
                                                    <img src={`https://img.youtube.com/vi/${helper.getVideoId(item.url)}/hqdefault.jpg`} alt=""/>
                                                </div>
                                                <div className="video-item__title">{item.title}</div>
                                            </div>
                                        ))}

                                        { screenWidth < 650 && <MobileMore blank={true} link={this.state.urlvideos} title={'все видео'} /> }
                                    </Slider>

                                    <div className="text-right pt-4 d-none d-sm-block">
                                        <a href={this.state.urlvideos} target="_blank" rel="noopener noreferrer" className="common-button">все видео</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {this.state.popup.isShow &&
                        <MagnificPopup
                            videoId={this.state.popup.videoId}
                            videoTitle={this.state.popup.title}
                            onClose={this.closePopup}
                        />}
                    </section>
                )}
            </ScreenWidth.Consumer>
        );
    }
}
