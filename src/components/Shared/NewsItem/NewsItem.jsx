import React from 'react';
import { NavLink } from "react-router-dom";
import cn from 'classnames';

import styles from './NewsItem.module.scss';

import type { News } from '../../../types';

import helper from '../../../helpers';

type Props = {
    news: News,
    small?: boolean,
    direction?: boolean
}

export default function NewsItem(props: Props) {
    return (
        <NavLink to={`/news/${props.news.id}`} className={cn(styles.item, { [styles.direction]: props.direction })}>
            <div className={cn(styles.image, { [styles.small]: props.small })}>
                <img src={process.env.REACT_APP_API_BASE_URL + props.news.image} alt=""/>
            </div>
            <div className={styles.title}><span>{props.news.title}</span></div>
            <div className={cn('common-date-block', styles.date)}>{helper.convert2LL(props.news.date)}</div>
        </NavLink>
    );
}
