// @flow

import React from 'react';
import styles from './FooterContacts.module.scss';
import type { Contacts } from '../../../types'

import phone from './images/phone.svg';
import mail from './images/mail.svg';
import location from './images/location.svg';

function FooterContacts(props: Contacts) {
    return (
        <div className={styles.block}>
            <a href={"tel:" + props.phone} className={styles.item}>
                <span className={styles.icon}>
                    <img src={phone} alt=""/>
                </span>
                <span className={styles.text}>{props.phone}</span>
            </a>

            <a href={"mailto:" + props.email} className={styles.item}>
                <span className={styles.icon}>
                    <img src={mail} alt=""/>
                </span>
                <span className={styles.text}>{props.email}</span>
            </a>

            <a href={props.map} target='_blank' rel="noopener noreferrer" className={styles.item}>
                <span className={styles.icon}>
                    <img src={location} alt=""/>
                </span>
                <span className={styles.text}>{props.address}</span>
            </a>
        </div>
    );
}

export default FooterContacts;
