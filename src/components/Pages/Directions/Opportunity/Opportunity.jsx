import React from 'react';
import { Helmet } from "react-helmet";
import type { Opportunity } from '../../../../types';
import opportunityService from '../../../../services/opportunity.service';
import OpportunityItem from '../../../Shared/OpportunityItem';

type State = {
    opportunityList: Array<Opportunity>,
    isShowMore: boolean
}

export default class OpportunityTab extends React.Component<{}, State> {
    constructor(props) {
        super(props);

        this.state = {
            opportunityList: [],
            isShowMore: true
        }
    }

    componentDidMount() {
        this.getData();
    }

    componentDidUpdate(prevProps) {
        if(prevProps.location.pathname !== this.props.location.pathname) {
            this.getData();
            this.setState({ isShowMore: true })
        }
    }

    getData = (offset = 0, filter) => {
        if(this.props.match.params.filter) {
            opportunityService.getFilteredList(this.props.match.params.filter, offset + this.props.location.search)
                .then(response => {
                    if (response.data.length) {
                        this.setState({ opportunityList: response.data })
                    } else {
                        this.setState({ opportunityList: response.data, isShowMore: false })
                    }
                });
        } else {
            opportunityService.getDirectionOpportunity(this.props.match.params.id, `0${this.props.location.search}`)
                .then(response => {
                    if (response.data.length) {
                        this.setState({ opportunityList: response.data })
                    } else {
                        this.setState({ opportunityList: response.data, isShowMore: false })
                    }
                })
        }
    };

    handleShowMore = () => {
        if(this.props.match.params.filter) {
            opportunityService.getFilteredList(this.props.match.params.filter, this.state.opportunityList.length + this.props.location.search)
                .then(response => {
                    if(response.data.length) {
                        this.setState(state => ({
                            ...state,
                            opportunityList: [...state.opportunityList, ...response.data]
                        }))
                    } else {
                        this.setState({ isShowMore: false })
                    }
                });
        } else {
            opportunityService.getDirectionOpportunity(this.props.match.params.id,this.state.opportunityList.length + this.props.location.search)
                .then(response => {
                    if(response.data.length) {
                        this.setState(state => ({
                            ...state,
                            opportunityList: [...state.opportunityList, ...response.data]
                        }))
                    } else {
                        this.setState({ isShowMore: false })
                    }
                })
        }
    };

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Возможности - ЗОЖ и безопасность</title>
                </Helmet>

                <div className="direction-page__block">
                    <div className="row mb-4">
                        {this.state.opportunityList.map(item => (
                            <div className='col-sm-6 col-12 mb-3' key={item.id}>
                                <OpportunityItem {...item} />
                            </div>
                        ))}
                    </div>

                    {this.state.isShowMore && <button className={'show-more'} onClick={() => this.handleShowMore()}>Показать еще</button>}
                </div>
            </React.Fragment>
        )
    }
}
