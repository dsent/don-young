import moment from 'moment';
import 'moment/locale/ru';

export default {
    convert2LL(utcDate: string) {
        return moment(utcDate).format('D MMMM YYYY');
    },

    convert2DatePicker(utcDate: string) {
        if(utcDate) {
            return moment(utcDate).format('D.M.YYYY');
        } else {
            return null;
        }

    },

    getVideoId(url: ?string) {
        if(url) {
            const urlData = new URL(url);
            return urlData.searchParams.get('v');
        } else {
            return '';
        }

    },

    getSearchQuery(url: string, param: string) {
        if(url) {
            const splitData = new URLSearchParams(url);
            return decodeURI(splitData.get(param));
        } else {
            return "";
        }
    },

    convertObject2Get(obj) {
        return obj ? '?' + Object.keys(obj).map(key => `${key}=${obj[key]}`).join('&') : '';
    }
}
