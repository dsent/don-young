import React from 'react';
import { NavLink } from "react-router-dom";

import directionsService from '../../../../../services/directions.service';
import type { Direction } from "../../../../../types";

import styles from './DirectionListMobile.module.scss'

type State = {
    list: Array<Direction>
}

export default class DirectionListMobile extends React.Component<{}, State> {
    constructor(props) {
        super(props);

        this.state = {
            list: []
        }
    }

    componentDidMount(): void {
        directionsService.getMainDirection()
            .then(response => {
                this.setState({ list: response.data })
            })
    }

    render() {
        return (
            <div className="direction-list">
                {this.state.list.map(item => (
                    <NavLink
                        to={`/direction/${item.id}/about`}
                        className={styles.item}
                        key={item.id}
                    >
                        <span>{item.title}</span>
                    </NavLink>
                ))}
            </div>
        )
    }
}
