import React from 'react';
import { Helmet } from "react-helmet";
import { Dropdown, Modal } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import cn from "classnames";

import OpportunityItem from "../../Shared/OpportunityItem";
import FilterByDate from '../../Shared/FilterByDate'
import FilterByDirection from '../../Shared/FilterByDirection'

import opportunityService from "../../../services/opportunity.service";

import { Opportunity } from '../../../types'

import { ReactComponent as FiltersIcon } from "../../../assets/images/filters-icon.svg";
import ScrollBackButton from "../../Shared/ScrollBackButton";

type State = {
    opportunityList: Array<Opportunity>,
    hubModal: {
        isShow: boolean
    },
    isShowMore: boolean,
    isLoading: boolean
}

export default class OpportunityList extends React.Component<{}, State> {
    constructor(props) {
        super(props);

        this.state = {
            opportunityList: [],
            hubModal: {
                isShow: false
            },
            isShowMore: true,
            isLoading: false
        }
    }

    componentDidMount(): void {
        this.getData();
    }

    componentDidUpdate(prevProps) {
        if((prevProps.location.pathname !== this.props.location.pathname) || (prevProps.location.search !== this.props.location.search)) {
            this.getData();
            this.setState({ isShowMore: true })
        }
    }

    getData = (offset = 0, filter = false) => {
        this.setState({ isLoading: true });

        if(this.props.match.params.filter) {
            if(filter) {
                opportunityService.getFilteredList(this.props.match.params.filter, `0${offset}`)
                    .then(response => {
                        this.setState({ opportunityList: response.data, isLoading: false })
                    });
            } else {
                opportunityService.getFilteredList(this.props.match.params.filter, offset + this.props.location.search)
                    .then(response => {
                        this.setState({ opportunityList: response.data, isLoading: false })
                    });
            }

        } else {
            if(filter) {
                opportunityService.getAllOpportunity(offset)
                    .then(response => {
                        this.setState({ opportunityList: response.data, isLoading: false })
                    })
            } else {
                opportunityService.getAllOpportunity(`0${this.props.location.search}`)
                    .then(response => {
                        this.setState({ opportunityList: response.data, isLoading: false })
                    })
            }

        }
    };

    handleShowMore = () => {
        this.setState({ isLoading: true });
        if(this.props.match.params.filter) {
            opportunityService.getFilteredList(this.props.match.params.filter, this.state.opportunityList.length + this.props.location.search)
                .then(response => {
                    if(response.data.length) {
                        this.setState(state => ({
                            ...state,
                            isLoading: false,
                            opportunityList: [...state.opportunityList, ...response.data]
                        }))
                    } else {
                        this.setState({ isShowMore: false, isLoading: false })
                    }
                });
        } else {
            opportunityService.getAllOpportunity(this.state.opportunityList.length + this.props.location.search)
                .then(response => {
                    if(response.data.length) {
                        this.setState(state => ({
                            ...state,
                            isLoading: false,
                            opportunityList: [...state.opportunityList, ...response.data]
                        }))
                    } else {
                        this.setState({ isShowMore: false, isLoading: false })
                    }
                })
        }
    };

    onToggleModalHub = (state) => {
        this.setState(prevState => ({
            ...prevState,
            hubModal: {
                ...prevState.hubModal,
                isShow: state === undefined ? !this.state.hubModal.isShow : state
            }
        }));
    };

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Возможности</title>
                </Helmet>

                <ScrollBackButton/>

                <div className="opportunity-page">
                    <div className="container">
                        <div className="d-flex align-items-center justify-content-between">
                            <h1 className={'main-title'}>Возможности</h1>

                            <button className={cn('mobile-filter', { 'mobile-filter--active': this.props.location.search })} onClick={() => this.onToggleModalHub()}>
                                <FiltersIcon />
                            </button>

                            <div className="actual-realize-buttons actual-realize-buttons--flagship">
                                <NavLink
                                    to={'/opportunity/active'}
                                    className={cn({'active': this.props.match.params.filter === 'active'})}
                                >
                                    <span>актуальные</span>
                                </NavLink>
                                <NavLink
                                    to={'/opportunity/completed'}
                                    className={cn({'active': this.props.match.params.filter === 'completed'})}
                                >
                                    <span>реализованные</span>
                                </NavLink>
                            </div>

                            <Dropdown className={"filter-dropdown"}>
                                <Dropdown.Toggle id={'newsFilter'}>
                                    <span>Применить фильтр</span>
                                </Dropdown.Toggle>

                                <Dropdown.Menu>
                                    <FilterByDate buttonClass={'dropdown-item'} onGetData={this.getData} />
                                    <FilterByDirection buttonClass={'dropdown-item'} onGetData={this.getData} />
                                    {this.props.location.search && <NavLink as="button" className={'dropdown-item'} activeClassName={'opportunity-active'} to={'/opportunity'}>Сбросить</NavLink>}
                                </Dropdown.Menu>
                            </Dropdown>
                        </div>

                        <div className="row opportunity-list">
                            {this.state.opportunityList.map(item => (
                                <div className='col-xl-4 col-md-6 col-12 mb-md-3' key={item.id}>
                                    <OpportunityItem {...item} />
                                </div>
                            ))}
                        </div>

                        {this.state.isShowMore && <button className={cn('show-more', { 'isLoading': this.state.isLoading })} onClick={() => this.handleShowMore()}>Показать еще</button>}
                    </div>
                </div>

                {/*---------------------------------------------------*/}

                <Modal
                    show={this.state.hubModal.isShow}
                    onHide={() => this.onToggleModalHub(false)}
                    dialogClassName="common-modal-dialog"
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Применить фильтр</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <div className="hub-modal">
                            <FilterByDate
                                buttonClass={'hub-modal__btn'}
                                onCloseHub={this.onToggleModalHub}
                                onGetData={this.getData}
                            />
                            <FilterByDirection
                                buttonClass={'hub-modal__btn'}
                                onCloseHub={this.onToggleModalHub}
                                onGetData={this.getData}
                            />
                            <NavLink
                                onClick={() => this.onToggleModalHub(false)}
                                to={'/news'}
                                className={'hub-modal__btn hub-modal__btn--reset'}
                            >Сбросить</NavLink>
                        </div>
                    </Modal.Body>
                </Modal>
            </React.Fragment>

        );
    }
}
