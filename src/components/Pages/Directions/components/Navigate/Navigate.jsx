import React from 'react';
import { NavLink } from "react-router-dom";

import styles from "./Navigate.module.scss";

type Props = {
    id: number,
    about?: boolean,
    news?: boolean,
    opportunity?: boolean,
}

export default function Navigate(props: Props) {
    const state = [
        {
            'code': 'about',
            'title': 'О направлении'
        },
        {
            'code': 'news',
            'title': 'Новости'
        },
        {
            'code': 'opportunity',
            'title': 'Возможности',
            // 'children': [
            //     {
            //         'code': 'opportunity/active',
            //         'title': 'Актуальные'
            //     },
            //     {
            //         'code': 'opportunity/completed',
            //         'title': 'Реализованные'
            //     },
            // ]
        },
    ];

    return (
        <div className={styles.block}>
            {state.map(item => (
                <div className={styles.item} key={item.code}>
                    <NavLink to={`/direction/${props.id}/${item.code}`} activeClassName={styles.active}>
                        <span>{item.title}</span>
                    </NavLink>

                    {item.children &&
                        <div className={styles.dropdown}>
                            {item.children.map(dItem => (
                                <NavLink to={`/direction/${props.id}/${dItem.code}`} key={dItem.code}>
                                    <span>{dItem.title}</span>
                                </NavLink>
                            ))}
                        </div>
                    }
                </div>
            ))}
        </div>
    );
}
