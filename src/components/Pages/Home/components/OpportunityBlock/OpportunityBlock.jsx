import React from 'react';
import { Link, NavLink } from "react-router-dom";
import type { Opportunity } from "../../../../../types";
import opportunityService from '../../../../../services/opportunity.service';

import OpportunityItem from '../../../../Shared/OpportunityItem';

type State = {
    opportunityList: Array<Opportunity>
}

export default class OpportunityBlock extends React.Component<{}, State> {
    constructor(props) {
        super(props);

        this.state = {
            opportunityList: []
        }
    }

    componentDidMount(): void {
        opportunityService.getMainOpportunity()
            .then(response => {
                this.setState({ opportunityList: response.data })
            })
    }

    render() {
        return (
            <section className="opportunity-block">
                <div className="container">
                    <div className="d-md-flex justify-content-between align-items-end">
                        <h2 className="common-title">возможности</h2>
                        <div className="actual-realize-buttons mb-4">
                            <NavLink className='active' to={"/opportunity/active"}>
                                <span>актуальные</span>
                            </NavLink>
                            <NavLink to={"/opportunity/completed"}>
                                <span>реализованные</span>
                            </NavLink>
                        </div>
                    </div>

                    <div className="row opportunity-list">
                        {this.state.opportunityList.map(item => (
                            <div className='col-xl-4 col-md-6 col-12 mb-md-3' key={item.id}>
                                <OpportunityItem {...item} />
                            </div>
                        ))}
                    </div>

                    <div className="text-right">
                        <Link className="common-button mt-4" to="/opportunity/active">Все возможности</Link>
                    </div>
                </div>
            </section>
        )
    }
}
