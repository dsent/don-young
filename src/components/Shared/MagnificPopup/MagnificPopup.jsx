import React from 'react';
import styles from './MagnificPopup.module.scss';

type Props = {
    videoId: string,
    videoTitle: string,
    onClose: () => void
}

export default class MagnificPopup extends React.Component<Props> {
    closeModal() {
        this.props.onClose();
    }

    render() {
        return (
            <div className={styles.wrap}>
                <div className={styles.container}>
                    <div className={styles.content}>
                        <button
                            title="Закрыть"
                            type="button"
                            className={styles.close}
                            onClick={() => this.closeModal()}
                        >×</button>
                        <div className="embed-responsive embed-responsive-16by9">
                            <iframe
                                className="embed-responsive-item"
                                title={this.props.videoTitle}
                                src={`https://www.youtube.com/embed/${this.props.videoId}?rel=0`}
                                allowFullScreen
                            />
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}
