import React from 'react';
import type { Document } from '../../../../../../types'
import docsIcon from "../../../../../../assets/images/docs-icon.svg";

export default function DocumentsBlock(props: Document) {
    return (
        <div className="doc-list__item">
            <a href={process.env.REACT_APP_API_BASE_URL + props.file} download>
                <img src={docsIcon} alt=""/>
                <span>{props.title}</span>
            </a>
        </div>
    )
}
