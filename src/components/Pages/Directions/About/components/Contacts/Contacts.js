import React from 'react';

import type { People } from '../../../../../../types'

import mailIcon from "../../../../../../assets/images/mail-icon.svg";
import phoneIcon from "../../../../../../assets/images/phone-icon.svg";

export default function ContactsBlock(props: People) {
    return (
        <div className="committee-personal-list__item">
            <div className="committee-personal-list__image">
                <img src={process.env.REACT_APP_API_BASE_URL + props.image} alt=""/>
            </div>
            <div className="committee-personal-list__content">
                <div className="committee-personal-list__name">{props.name}</div>
                <div className="committee-personal-list__job">{props.position}</div>
                <div className="contacts">
                    {props.email &&
                        <div className="contacts__item contacts__item--committee-personal">
                            <div className="contacts__icon">
                                <img src={mailIcon} alt=""/>
                            </div>
                            <span>{props.email}</span>
                        </div>
                    }
                    {props.phone &&
                        <div className="contacts__item contacts__item--committee-personal">
                            <div className="contacts__icon">
                                <img src={phoneIcon} alt=""/>
                            </div>
                            <span>{props.phone}</span>
                        </div>
                    }
                </div>
            </div>
        </div>
    )
}
