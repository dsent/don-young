import http from './http.service';

export default {
    getAllMedia() {
        return http.get(`${process.env.REACT_APP_API_BASE_URL}/api/main/media/`)
    }
}
