import React from 'react';
import cn from 'classnames';
import styles from './ScrollBackButton.module.scss'

export default class ScrollBackButton extends React.Component {
    state = {
        buttonState: 'back'
    };

    componentDidMount() {
        window.addEventListener('scroll', this.handleScroll, true);
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll = () => {
        if(window.scrollY > 100) {
            this.setState({ buttonState: 'top' })
        } else {
            this.setState({ buttonState: 'back' })
        }
    };

    scrollTop = () => {
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    };

    render() {
        return (
            <React.Fragment>
                {this.state.buttonState === 'back' &&
                    <div className={cn(styles.buttonWrapper, { [styles.buttonWrapperOffset]: this.props.isMoreOffset })} onClick={() => window.history.back()}>
                        <div className='container'>
                            <div className="d-flex align-items-center">
                                <div className={styles.buttonIconBack} />
                                <span>Назад</span>
                            </div>
                        </div>
                    </div>
                }

                {this.state.buttonState === 'top' &&
                    <div className={styles.buttonWrapper} onClick={() => this.scrollTop()}>
                        <div className='container'>
                            <div className="d-flex align-items-center">
                                <div className={styles.buttonIconTop} />
                                <span>Наверх</span>
                            </div>
                        </div>
                    </div>
                }
            </React.Fragment>
        )
    }
}
