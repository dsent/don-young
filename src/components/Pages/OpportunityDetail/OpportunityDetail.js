import React from 'react';
import { Helmet } from "react-helmet";
import { NavLink } from "react-router-dom";
import cn from "classnames";

import {
    VKShareButton,
    VKIcon,
    FacebookShareButton,
    FacebookIcon,
    TwitterShareButton,
    TwitterIcon,
    OKShareButton,
    OKIcon,
} from 'react-share';

import opportunityService from '../../../services/opportunity.service';

import addressIcon from '../../../assets/images/address-icon.svg'
import calendarIcon from '../../../assets/images/calendar-icon.svg'
import timeIcon from '../../../assets/images/time-icon.svg'
import ScrollBackButton from "../../Shared/ScrollBackButton";

export default class OpportunityDetail extends React.Component {
    state = {
        opportunity: null,
        nextOpportunity: null,
        previousOpportunity: null,
        similarOpportunity: []
    };

    componentDidMount(): void {
        this.getData();
    }

    componentDidUpdate(prevProps) {
        if(prevProps.location.pathname !== this.props.location.pathname) {
            this.getData();
        }
    }

    getData = () => {
        opportunityService.getOpportunityDetail(this.props.match.params.id)
            .then(response => {
                this.setState({ ...response.data });
            })
    };

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>{this.state.opportunity && this.state.opportunity.title}</title>
                    <meta property="og:title" content={this.state.opportunity && this.state.opportunity.title} />
                    {this.state.opportunity &&<meta property="og:image" content={process.env.REACT_APP_API_BASE_URL + this.state.opportunity.image} />}
                </Helmet>

                <ScrollBackButton/>

                {this.state.opportunity &&
                    <div className="news-detail-page">
                        <div className="container">
                            <div className="row">
                                <div className="col">
                                    <div className="news-detail">
                                        <h2>{this.state.opportunity.title}</h2>

                                        <div className="news-detail__main-image">
                                            <img src={ process.env.REACT_APP_API_BASE_URL + this.state.opportunity.image } alt=""/>

                                            <div className="opportunity-info">

                                                {this.state.opportunity.date &&
                                                    <div className="opportunity-info__item">
                                                        <div className="opportunity-info__icon">
                                                            <img src={calendarIcon} alt=""/>
                                                        </div>
                                                        <span>{this.state.opportunity.date}</span>
                                                    </div>
                                                }

                                                {this.state.opportunity.time &&
                                                    <div className="opportunity-info__item">
                                                        <div className="opportunity-info__icon">
                                                            <img src={timeIcon} alt=""/>
                                                        </div>
                                                        <span>{this.state.opportunity.time}</span>
                                                    </div>
                                                }

                                                {this.state.opportunity.address &&
                                                    <div className="opportunity-info__item">
                                                        <div className="opportunity-info__icon">
                                                            <img src={addressIcon} alt=""/>
                                                        </div>
                                                        <a href={this.state.opportunity.map} target='_blank' rel="noopener noreferrer">{this.state.opportunity.address}</a>
                                                    </div>
                                                }
                                            </div>
                                        </div>

                                        <div className="news-detail__content">
                                            {this.state.opportunity.blocks.map(block => (
                                                <div className={cn(
                                                    'news-detail-block',
                                                    { 'news-detail-block--top-line': block.topLine },
                                                    { 'news-detail-block--bottom-line': block.bottomLine },
                                                )} key={block.title}>
                                                    {block.newsfile &&
                                                    <div className="news-detail-block__media">
                                                        {this.renderMedia(block.newsfile)}
                                                        <div className="news-detail-block__media-label">
                                                            {block.newsfile.label}
                                                        </div>
                                                    </div>
                                                    }
                                                    {block.text &&
                                                    <div className="news-detail-block__text">
                                                        <h3>{block.title}</h3>
                                                        <div dangerouslySetInnerHTML={{ __html: block.text }} />
                                                    </div>
                                                    }
                                                </div>
                                            ))}
                                        </div>

                                        <div className="news-detail__tags">
                                            <VKShareButton className="mr-2" url={window.location.href}>
                                                <VKIcon size={30} round={true}/>
                                            </VKShareButton>

                                            <FacebookShareButton className="mr-2" url={window.location.href}>
                                                <FacebookIcon size={30} round={true}/>
                                            </FacebookShareButton>

                                            <TwitterShareButton className="mr-2" url={window.location.href}>
                                                <TwitterIcon size={30} round={true}/>
                                            </TwitterShareButton>

                                            <OKShareButton className="mr-2" url={window.location.href}>
                                                <OKIcon size={30} round={true}/>
                                            </OKShareButton>
                                        </div>

                                        <div className="news-detail__tags">
                                            {this.state.opportunity.trends.map(item => (
                                                <NavLink key={item.id} to={`/direction/${item.id}/opportunity`}>{item.title}</NavLink>
                                            ))}
                                        </div>
                                    </div>
                                </div>

                                <div className="col col-sidebar">
                                    <div className="position-sticky pb-5">
                                        <div className="row">

                                            {this.state.nextOpportunity &&
                                                <div className="col-12">
                                                    <div className="sidebar-title">следующая возможность</div>
                                                    <NavLink className="sidebar-news-block" to={`/opportunity/${this.state.nextOpportunity.id}`}>{this.state.nextOpportunity.title}</NavLink>
                                                </div>
                                            }

                                            {this.state.previousOpportunity &&
                                                <div className="col-12">
                                                    <div className="sidebar-title">предыдущая возможность</div>
                                                    <NavLink
                                                        className="sidebar-news-block"
                                                        to={`/opportunity/${this.state.previousOpportunity.id}`}>{this.state.previousOpportunity.title}</NavLink>
                                                </div>
                                            }

                                            {this.state.similarOpportunity.length > 0 &&
                                                <div className="col-12">
                                                    <div className="sidebar-title">похожие возможности</div>

                                                    <div className="row">
                                                        {this.state.similarOpportunity.map(item => (
                                                            <div className="col-12" key={item.id}>
                                                                <NavLink className="sidebar-news-block" to={`/opportunity/${item.id}`}>{item.title}</NavLink>
                                                            </div>
                                                        ))}
                                                    </div>
                                                </div>
                                            }
                                        </div>

                                        <NavLink className={'sidebar-more'} to={`/opportunity`}>Все возможности</NavLink>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
            </React.Fragment>

        );
    }
}
