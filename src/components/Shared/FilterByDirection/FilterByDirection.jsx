import React from 'react';
import { withRouter } from 'react-router-dom';
import cn from 'classnames';
import {Modal} from "react-bootstrap";

import directionService from '../../../services/directions.service';

import type { Direction } from "../../../types";
import helper from "../../../helpers";

type Props = {
    buttonClass: string,
    onCloseHub?: (?boolean) => void,
    onGetData: (?{ from: string, to: string }) => void
}

type State = {
    isShow: boolean,
    directionList: Array<Direction>,
}

class FilterByDirection extends React.Component<Props, State> {
    constructor(props) {
        super(props);

        this.state = {
            isShow: false,
            directionList: []
        }
    }

    componentDidMount(): void {
        directionService.getMainDirection()
            .then(response => {
                this.setState({ directionList: response.data })
            })
    }

    onToggleModal = (state) => {
        if (!state && this.props.onCloseHub) {
            this.props.onCloseHub(false);
        }

        this.setState({ isShow: state === undefined ? !this.state.isShow : state });
    };

    selectDirection = (id: number) => {
        this.onToggleModal(false);

        this.props.history.push({
            pathname: this.props.location.pathname,
            search: `direction=${id}`
        })

        this.props.onGetData(`0${helper.convertObject2Get({ direction: id })}`, true);
    };

    render(){
        const urlParams = new URLSearchParams(this.props.location.search);

        return (
            <React.Fragment>
                <button className={this.props.buttonClass} onClick={() => this.onToggleModal(true)}>По направлению</button>

                <Modal
                    show={this.state.isShow}
                    onHide={() => this.onToggleModal(false)}
                    dialogClassName="common-modal-dialog"
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Выберите направление</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <div className="overflow-auto">
                            <div className="direction-list">
                                {this.state.directionList.map(item => (
                                    <button key={item.id} className={cn({ 'active': urlParams.get('direction') === item.id.toString() })} onClick={() => this.selectDirection(item.id)}>
                                        {item.title}
                                    </button>
                                ))}
                            </div>
                        </div>
                    </Modal.Body>
                </Modal>
            </React.Fragment>
        )
    }
}

export default withRouter(FilterByDirection);
