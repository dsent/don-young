import React from 'react';
import { Redirect, withRouter } from "react-router-dom";

import styles from './SearchBlock.module.scss';
import { Modal } from 'react-bootstrap';

import type { ReactRef } from '../../../types';

import { ReactComponent as Search } from './images/search.svg';

type State = {
    isShow: boolean,
    input: ReactRef<HTMLInputElement>,
    redirectPath: ?string
}

class SearchBlock extends React.Component<{}, State> {
    state = {
        isShow: false,
        input: React.createRef(),
        redirectPath: null
    };

    onToggleModal = (state: ?boolean): void => {
        this.setState({ isShow: state === undefined ? !this.state.isShow : state});
    };

    handleSubmitForm = (e: Event) => {
        e.preventDefault();

        const page = this.props.location.pathname.split('/');
        this.setState({ isShow: false, redirectPath: `/search?q=${this.state.input.current.value}&p=${page[1]}` });
    };

    renderRedirect = () => {
        if (this.state.redirectPath) {
            return <Redirect to={this.state.redirectPath} />
        }
    };

    render() {
        return (
            <React.Fragment>
                {this.renderRedirect()}
                <Modal
                    show={this.state.isShow}
                    onHide={() => this.onToggleModal(false)}
                    dialogClassName="common-modal-dialog"
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Поиск по сайту</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <form onSubmit={this.handleSubmitForm}>
                            <input type="search" ref={this.state.input} placeholder="Вводимый текст..." />
                        </form>
                    </Modal.Body>
                </Modal>

                <button className={styles.headerTopSearch} onClick={() => this.onToggleModal()}>
                    <Search />
                </button>
            </React.Fragment>
        );
    }
}

export default withRouter(SearchBlock);
