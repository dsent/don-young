// @flow

import type { People, Partners } from '../index';
import type { Document } from './document.type';

export type AboutDirection = {
    id: number,
    title: string,
    text: string,
    imageActive: string,
    imageInactive: string,
    trendscontacts_set: Array<People>,
    trendspartners_set: Array<Partners>,
    trendsdocuments_set: Array<Document>
}
