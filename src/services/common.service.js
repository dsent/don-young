import http from './http.service';

export default {
    getCommonData() {
        return http.get(`${process.env.REACT_APP_API_BASE_URL}/api/common/`)
    }
}
