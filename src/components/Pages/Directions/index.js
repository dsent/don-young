import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import directionsService from '../../../services/directions.service'
import _ from 'lodash';

import DirectionsAbout from './About';
import DirectionsOpportunity from './Opportunity';
import DirectionsNews from './News';
import type { Direction } from "../../../types";
import {Modal} from "react-bootstrap";
import DirectionListMobile from "./components/DirectionListMobile";
import Navigate from "./components/Navigate";
import DirectionList from "./components/DirectionList";
import ScrollBackButton from "../../Shared/ScrollBackButton";

type State = {
    directions: Array<Direction>,
    modal: {
        isShow: boolean
    }
}

export default class Directions extends React.Component<{}, State> {
    state = {
        modal: {
            isShow: false
        },
        directions: []
    };

    componentDidMount(): void {
        directionsService.getMainDirection()
            .then(response => {
                this.setState({ directions: response.data })
            })
    }

    componentDidUpdate(prevProps) {
        if((prevProps.location.pathname !== this.props.location.pathname) && !this.props.match.params.id) {
            this.props.history.push({
                pathname: `/direction/${this.state.directions[0].id}/about`
            });
        }
    }

    redirectToFirstDirection() {
        debugger
        if(this.props.match.params.id) {
            if(this.props.location.search) {
                const path = this.props.location.search.split('=');
                return <Redirect to={`/direction/${this.props.match.params.id}/${path[1]}`}/>
            } else {
                return <Redirect to={`/direction/${this.props.match.params.id}/about`}/>
            }

        } else {
            if (this.state.directions.length) {
                return <Redirect to={`/direction/${this.state.directions[0].id}/about`}/>
            } else {
                return null;
            }
        }
    }

    onToggleModal = (state) => {
        this.setState(prevState => ({
            ...prevState,
            modal: {
                ...prevState.modal,
                isShow: state === undefined ? !this.state.modal.isShow : state
            }
        }));
    };

    render() {
        const direction = _.find(this.state.directions, { id: +this.props.match.params.id });
        return (
            <React.Fragment>
                <div className="mobile-directions">
                    <button className="mobile-directions__button" onClick={() => this.onToggleModal()}>Выберите направление</button>
                </div>

                <ScrollBackButton isMoreOffset={true}/>

                {/*{this.redirectToFirstDirection()}*/}

                <div className="direction-page">
                    <div className="container">
                        {this.state.directions.length && this.props.match.params.id &&
                            <React.Fragment>
                                <h1 className={'main-title'}>{_.find(this.state.directions, { id: +this.props.match.params.id }).title}</h1>

                                <div className="row">
                                    <div className="col">
                                        <Navigate id={this.props.match.params.id} />
                                        <Switch>
                                            <Route
                                                path={"/direction/:id/about"}
                                                render={(props) => <DirectionsAbout {...props} title={direction.title} />} />
                                            <Route
                                                exact
                                                path={"/direction/:id/opportunity"}
                                                render={(props) => <DirectionsOpportunity {...props} title={direction.title} />} />
                                            <Route
                                                path={"/direction/:id/opportunity/:filter"}
                                                render={(props) => <DirectionsOpportunity {...props} title={direction.title} />} />
                                            <Route
                                                path={"/direction/:id/news"}
                                                render={(props) => <DirectionsNews {...props} title={direction.title} />}  />

                                            <Redirect to={`/direction/${this.props.match.params.id}/about`}/>
                                        </Switch>
                                    </div>
                                    <div className="col col-sidebar">
                                        <DirectionList list={this.state.directions} />
                                    </div>
                                </div>
                            </React.Fragment>
                        }
                    </div>
                </div>
                <Modal
                    show={this.state.modal.isShow}
                    onHide={() => this.onToggleModal(false)}
                    dialogClassName="common-modal-dialog"
                >
                    <Modal.Header closeButton>
                        <Modal.Title>Выберите направление</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <div className="overflow-auto">

                                <DirectionListMobile />
                        </div>
                    </Modal.Body>
                </Modal>
            </React.Fragment>
        )
    }
}
