// @flow
import type { People } from './people.type'

export type Committee = {
    id: number,
    title: string,
    isOpen: boolean,
    aboutblockpeople_set: Array<People>
}
