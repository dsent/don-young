import React from 'react';
import { Helmet } from "react-helmet";

import type { News } from '../../../../types'

import newsService from '../../../../services/news.service'
import NewsItem from "../../../Shared/NewsItem";

type State = {
    news: Array<News>,
    isShowMore: boolean
}

export default class NewsTab extends React.Component<{}, State> {
    constructor(props) {
        super(props);

        this.state = {
            news: [],
            isShowMore: true
        }
    }

    componentDidMount(): void {
        newsService.getDirectionNews(this.props.match.params.id, 0)
            .then(response => {
                if (response.data.length) {
                    this.setState({ news: response.data })
                } else {
                    this.setState({ news: response.data, isShowMore: false })
                }

            })
    }

    handleShowMore = () => {
        newsService.getDirectionNews(this.props.match.params.id, this.state.news.length)
            .then(response => {
                if(response.data.length) {
                    this.setState(state => ({
                        news: [...state.news, ...response.data]
                    }))
                } else {
                    this.setState({ isShowMore: false })
                }

            });
    };

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>Новости - ЗОЖ и безопасность</title>
                </Helmet>

                <div className="direction-page__block">
                    <div className="row mb-4">
                        {this.state.news.map(item => (
                            <div className="col-sm-6 col-12 mb-3" key={item.id}>
                                <NewsItem news={item} small direction />
                            </div>
                        ))}
                    </div>

                    {this.state.isShowMore && <button className={'show-more'} onClick={() => this.handleShowMore()}>Показать еще</button>}
                </div>
            </React.Fragment>
        )
    }
}
