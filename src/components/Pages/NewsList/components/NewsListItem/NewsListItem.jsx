import React from 'react';
import { NavLink } from "react-router-dom";
import newsMore from "../../../../../assets/images/news-more.svg";
import type { News } from "../../../../../types";
import helper from '../../../../../helpers'

function NewsListItem(props: News) {
    const previewText = props.titlePreview && (props.titlePreview.length < 300 ? props.titlePreview : props.titlePreview.slice(0, 300) + '...');

    return (
        <NavLink to={`/news/${props.id}`} className="news-list__item">
            <div className="news-list__image">
                <img src={process.env.REACT_APP_API_BASE_URL + props.image} alt=""/>
            </div>
            <div className="news-list__content">
                <div className="news-list__date">{helper.convert2LL(props.date)}</div>
                <div className="news-list__title">{props.title}</div>
                <div className="news-list__preview-text">
                    <p>{previewText}</p>
                </div>


                <div className="text-right">
                    <div className="news-list__more">
                        <span>подробнее</span>
                        <img src={newsMore} alt=""/>
                    </div>
                </div>
            </div>
        </NavLink>
    )
}

export default NewsListItem;
