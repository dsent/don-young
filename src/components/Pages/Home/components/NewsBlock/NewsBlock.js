import React from 'react';
import { NavLink } from "react-router-dom";
import { ScreenWidth } from '../../../../../context'
import cn from 'classnames';

import Slider from 'react-slick';

import MainNews from "../MainNews";
import NewsItem from "../../../../Shared/NewsItem";
import MobileMore from '../../../../Shared/MobileMore';

import styles from './NewsBlock.module.scss'

import type { News } from "../../../../../types/news.type";
import newsService from '../../../../../services/news.service';

type State = {
    news: {
        main: Array<News>,
        important: Array<News>
    },
    carouselSettings: any
}

export default class NewsBlock extends React.Component<{}, State> {
    constructor(props) {
        super(props);

        this.state = {
            news: {
                main: [],
                important: [],
            },
            carouselSettings: {
                dots: true,
                arrows: false,
                infinite: false,
                slidesToShow: 2,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 550,
                        settings: {
                            slidesToShow: 1,
                        }
                    }
                ]
            }
        }
    }

    componentDidMount(): void {
        newsService.getMainNews()
            .then(response => {
                this.setState({ news: response.data })
            })
    }

    newsRender(screenWidth: number) {
        if(screenWidth > 767) {
            return (
                <div className={styles.block}>
                    <div className={cn(styles.item)}>
                        <MainNews news={this.state.news.main} />
                    </div>

                    {this.state.news.important.map((item, index) => (
                        <div className={cn(styles.item, {[styles.firstImportant]: index === 0})} key={item.id}>
                            <NewsItem news={item} />
                        </div>
                    ))}
                </div>
            )
        } else {
            return (
                <Slider className='news-slider-mobile slider-dotted-line' {...this.state.carouselSettings}>
                    {this.state.news.important.map(item => (
                        <NewsItem news={item} key={item.id} />
                    ))}

                    <MobileMore title={'все новости'} link={'/news'} blank={false} />
                </Slider>
            )
        }
    }

    render() {
        return(
            <ScreenWidth.Consumer>
                {({screenWidth}) => (
                    <section className="news-block">
                        <div className="container">
                            <div className="d-flex justify-content-between align-items-end">
                                <h2 className="common-title ">Новости</h2>
                                <NavLink to="/news" className="common-button mb-4 d-none d-lg-block">
                                    <span>все новости</span>
                                </NavLink>
                            </div>

                            {this.newsRender(screenWidth)}
                        </div>
                    </section>
                )}
            </ScreenWidth.Consumer>
        )
    }
}
