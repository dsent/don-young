// @flow

export type Video = {
    id: number,
    title: string,
    url: string
}
