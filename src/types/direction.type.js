// @flow

export type Direction = {
    id: number,
    title: string,
    imageActive: ?string,
    imageInactive: ?string,
    text: ?string
}
