// @flow

import { GET_COMMON_DATA } from '../constants/actionTypes/commonData';
import type { CommonData } from '../types'

const initialState = {
    social: {
        vk: '',
        twitter: ''
    },
    contacts: {
        phone: '',
        email: '',
        address: ''
    },
    massMedia: {
        phone: '',
        email: '',
        address: ''
    },
    menu: [],
    partners: []
};

export default (state: CommonData = initialState, action: any) => {
    switch (action.type) {
        case GET_COMMON_DATA:
            return {
                ...state,
                ...action.payload
            };

        default:
            return state;

    }
}
