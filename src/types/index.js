import type { Contacts } from './contacts.type';
import type { Social } from './social.types';
import type { Menu } from './menu.type';
import type { News } from './news.type';
import type { Flagship } from './flagship.type';
import type { Opportunity } from './opportunity.type';
import type { Direction } from './direction.type';
import type { Photo } from './photo.type';
import type { Video } from './video.type';
import type { Territory } from './territory.type';

import type { Partners } from './partners.type';

import type { CommonData } from './commonData.type'
import type { Media } from './media.type'

import type { People } from './about/people.type'
import type { Committee } from './about/committee.type'
import type { About } from './about/about.type'

import type { Document } from './direction/document.type'
import type { AboutDirection } from './direction/about.type'

import type { ReactRef } from './reactRef.type'

export type {
    Contacts,
    Social,
    Menu,
    News,
    Flagship,
    Opportunity,
    Direction,
    Photo,
    Video,
    Territory,

    Partners,

    CommonData,
    Media,

    People,
    Committee,
    About,

    Document,
    AboutDirection,

    ReactRef
}
