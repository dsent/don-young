// @flow

import type { Contacts } from './contacts.type';
import type { Social } from './social.types';
import type { Menu } from './menu.type';
import type { Partners } from './partners.type';

export type CommonData = {
    social: Social,
    contacts: Contacts,
    massMedia: Contacts,
    menu: Array<Menu>,
    partners: Array<Partners>,
    organizers: Array<Partners>,
    main: {
        copyright: string,
        banner: string
    }
}
