import http from './http.service';

export default {
    getMainOpportunity() {
        return http.get(`${process.env.REACT_APP_API_BASE_URL}/api/main/opportunities/`)
    },

    getCurrentOpportunity(params = '') {
        return http.get(`${process.env.REACT_APP_API_BASE_URL}/api/opportunities/active/list/${params}`)
    },

    getFinishedOpportunity(params = '') {
        return http.get(`${process.env.REACT_APP_API_BASE_URL}/api/opportunities/completed/list/${params}`)
    },

    getFilteredList(filter, params = '') {
        return http.get(`${process.env.REACT_APP_API_BASE_URL}/api/opportunities/${filter}/list/${params}`)
    },

    getAllOpportunity(params = '') {
        return http.get(`${process.env.REACT_APP_API_BASE_URL}/api/opportunities/all/list/${params}`)
    },

    getOpportunityDetail(id) {
        return http.get(`${process.env.REACT_APP_API_BASE_URL}/api/opportunities/detail/${id}`)
    },

    getDirectionOpportunity(id, params = '') {
        return http.get(`${process.env.REACT_APP_API_BASE_URL}/api/directions/${id}/opportunities/${params}`)
    },
}
