// @flow

export type ReactRef<T> = {
    current: ?T
}
