import React from 'react';
import styles from './Banner.module.scss';
import { ScreenWidth } from '../../../../../context'

export default class Banner extends React.Component {
    renderBanner(screenWidth: number) {
        if(screenWidth > 1010) {
            return (
                <div className={styles.banner} style={{ backgroundImage: `url(${process.env.REACT_APP_API_BASE_URL + this.props.banner})` }}>
                    <div className="container">
                        <img src={process.env.REACT_APP_API_BASE_URL + this.props.logo} alt=""/>
                    </div>

                </div>
            )
        } else {
            return (
                <div className={styles.mobileBanner}>
                    <img src={process.env.REACT_APP_API_BASE_URL + this.props.logo} alt=""/>
                </div>
            )
        }
    }

    render() {
        return(
            <ScreenWidth.Consumer>
                {({screenWidth}) => (
                    this.renderBanner(screenWidth)
                )}
            </ScreenWidth.Consumer>

        )
    }
}
