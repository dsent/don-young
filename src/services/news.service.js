import http from './http.service';

export default {
    getMainNews() {
        return http.get(`${process.env.REACT_APP_API_BASE_URL}/api/main/news/`);
    },

    getAllNews(params = '') {
        return http.get(`${process.env.REACT_APP_API_BASE_URL}/api/news/list/${params}`);
    },

    getDirectionNews(id, offset) {
        return http.get(`${process.env.REACT_APP_API_BASE_URL}/api/directions/${id}/news/${offset}`);
    },

    getMewsDetail(id) {
        return http.get(`${process.env.REACT_APP_API_BASE_URL}/api/news/detail/${id}`);
    }
}
