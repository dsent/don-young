import React from 'react';
import type { Partners } from "../../../../../../types";

export default function PartnersBlock(props: Partners) {
    return (
        <React.Fragment>
            {
                (props.url.length > 0) &&
                <a href={props.url} target='_blank' rel="noopener noreferrer" className="partners-list__item">
                    {props.image && (
                        <div className="partners-list__icon">
                            <img src={process.env.REACT_APP_API_BASE_URL + props.image} alt=""/>
                        </div>
                    )}
                    <span>{props.title}</span>
                </a>
            }

            {
                (props.url.length === 0) &&
                <span className="partners-list__item">
                    {props.image && (
                        <div className="partners-list__icon">
                            <img src={process.env.REACT_APP_API_BASE_URL + props.image} alt=""/>
                        </div>
                    )}
                    <span>{props.title}</span>
                </span>
            }
        </React.Fragment>
    )
}
