import React from 'react';
import { Helmet } from "react-helmet";
import { NavLink } from "react-router-dom";
import cn from 'classnames';

import {
    VKShareButton,
    VKIcon,
    FacebookShareButton,
    FacebookIcon,
    TwitterShareButton,
    TwitterIcon,
    OKShareButton,
    OKIcon,
} from 'react-share';

import helpers from '../../../helpers'
import newsService from '../../../services/news.service';

import type { News } from '../../../types'
import ScrollBackButton from "../../Shared/ScrollBackButton";

type AdditionalNews = {
    id: number,
    title: string
}

type State = {
    news: News | null,
    nextNews: AdditionalNews,
    previousNews: AdditionalNews,
    similarNews: Array<AdditionalNews>
}

export default class NewsDetail extends React.Component<{}, State> {
    constructor(props) {
        super(props);

        this.state = {
            news: null,
            nextNews: {},
            previousNews: {},
            similarNews: []
        }
    }

    componentDidMount(): void {
        this.getData();
    }

    componentDidUpdate(prevProps) {
        if(prevProps.location.pathname !== this.props.location.pathname) {
            this.getData();
        }
    }

    getData = () => {
        newsService.getMewsDetail(this.props.match.params.id)
            .then(response => {
                this.setState({ ...response.data });
            })
    };

    renderMedia = (file) => {
        if(file.ext === 'img') {
            return (
                <img src={process.env.REACT_APP_API_BASE_URL + file.file} alt={file.label} />
            )
        }
    };

    render() {
        return (
            <React.Fragment>
                <Helmet>
                    <title>{this.state.news && this.state.news.title}</title>
                    <meta property="og:title" content={this.state.news && this.state.news.title} />
                    {this.state.news &&<meta property="og:image" content={process.env.REACT_APP_API_BASE_URL + this.state.news.image} />}
                </Helmet>

                <ScrollBackButton/>

                {this.state.news &&
                <div className="news-detail-page">
                    <div className="container">
                        <div className="row">
                            <div className="col">
                                <div className="news-detail">
                                    <div className="news-detail__main-image">
                                        <img src={process.env.REACT_APP_API_BASE_URL + this.state.news.image} alt=""/>
                                    </div>

                                    <h2>{this.state.news.title}</h2>

                                    <div className="news-detail__date">{helpers.convert2LL(this.state.news.date)}</div>

                                    <div className="news-detail__content">
                                        {this.state.news.blocks.map(block => (
                                            <div className={cn(
                                                'news-detail-block',
                                                { 'news-detail-block--top-line': block.topLine },
                                                { 'news-detail-block--bottom-line': block.bottomLine },
                                            )} key={block.id}>
                                                {block.newsfile &&
                                                    <div className="news-detail-block__media">
                                                        {this.renderMedia(block.newsfile)}
                                                        <div className="news-detail-block__media-label">
                                                            {block.newsfile.label}
                                                        </div>
                                                    </div>
                                                }
                                                {block.text &&
                                                    <div className="news-detail-block__text">
                                                        <h3>{block.title}</h3>
                                                        <div dangerouslySetInnerHTML={{ __html: block.text }} />
                                                    </div>
                                                }
                                            </div>
                                        ))}
                                    </div>

                                    <div className="news-detail__tags">
                                        <VKShareButton className="mr-2" url={window.location.href}>
                                            <VKIcon size={30} round={true}/>
                                        </VKShareButton>

                                        <FacebookShareButton className="mr-2" url={window.location.href}>
                                            <FacebookIcon size={30} round={true}/>
                                        </FacebookShareButton>

                                        <TwitterShareButton className="mr-2" url={window.location.href}>
                                            <TwitterIcon size={30} round={true}/>
                                        </TwitterShareButton>

                                        <OKShareButton className="mr-2" url={window.location.href}>
                                            <OKIcon size={30} round={true}/>
                                        </OKShareButton>
                                    </div>

                                    <div className="news-detail__tags">
                                        {this.state.news.trends.map(item => (
                                            <NavLink key={item.id} to={`/direction/${item.id}/news`}>{item.title}</NavLink>
                                        ))}
                                    </div>
                                </div>
                            </div>

                            <div className="col col-sidebar">
                                <div className="position-sticky">
                                    <div className="row">
                                        {this.state.nextNews &&
                                            <div className="col-lg-12 col-sm-6">
                                                <div className="sidebar-news">
                                                    <div className="sidebar-title">следующая новость</div>
                                                    <NavLink className="sidebar-news-block" to={`/news/${this.state.nextNews.id}`}>{this.state.nextNews.title}</NavLink>
                                                </div>
                                            </div>
                                        }

                                        {this.state.previousNews &&
                                            <div className="col-lg-12 col-sm-6">
                                                <div className="sidebar-news">
                                                    <div className="sidebar-title">предыдущая новость</div>
                                                    <NavLink className="sidebar-news-block" to={`/news/${this.state.previousNews.id}`}>{this.state.previousNews.title}</NavLink>
                                                </div>
                                            </div>
                                        }

                                        {this.state.similarNews.length > 0 &&
                                            <div className="col-12">
                                                <div className="sidebar-news">
                                                    <div className="sidebar-title">похожие новости</div>

                                                    <div className="row">
                                                        {this.state.similarNews.map(item => (
                                                            <div className="col-12" key={item.id}>
                                                                <NavLink className="sidebar-news-block" to={`/news/${item.id}`}>{item.title}</NavLink>
                                                            </div>
                                                        ))}
                                                    </div>
                                                </div>
                                            </div>
                                        }
                                    </div>

                                    <NavLink className={'sidebar-more'} to={`/news`}>Все новости</NavLink>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                }
            </React.Fragment>
        );
    }
}
