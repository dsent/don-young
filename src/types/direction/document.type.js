// @flow

export type Document = {
    id: number,
    title: string,
    file: string
}
