import React from 'react';
import ReactDOM from 'react-dom';
import ReactGA from 'react-ga';

import { ScreenWidth } from './context';

import Layout from './components/Layout/Main';
import * as serviceWorker from './serviceWorker';
import { BrowserRouter } from 'react-router-dom';
import ScrollToTop from 'react-router-scroll-top'

const contextData = {
    screenWidth: window.innerWidth
};

ReactGA.initialize('UA-154760289-1');
ReactGA.pageview(window.location.pathname + window.location.search);

ReactDOM.render(
    <ScreenWidth.Provider value={contextData}>
        <BrowserRouter>
            <ScrollToTop>
                <Layout />
            </ScrollToTop>
        </BrowserRouter>
    </ScreenWidth.Provider>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
