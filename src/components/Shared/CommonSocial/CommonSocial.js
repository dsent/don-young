import React from 'react';
import type { Social } from '../../../types'

import vkImage from "./images/vk.svg";
import instagramImage from "./images/instagram.svg";
import facebookImage from "./images/facebook.svg";
import youtubeImage from "./images/youtube.svg";

function CommonSocial(props: Social) {
    return (
        <React.Fragment>
            <a href={props.vk} target="_blank" rel="noopener noreferrer">
                <img src={vkImage} alt=""/>
            </a>
            <a href={props.instagram} target="_blank" rel="noopener noreferrer">
                <img src={instagramImage} alt=""/>
            </a>
            <a href={props.facebook} target="_blank" rel="noopener noreferrer">
                <img src={facebookImage} alt=""/>
            </a>
            <a href={props.youtube} target="_blank" rel="noopener noreferrer">
                <img src={youtubeImage} alt=""/>
            </a>
        </React.Fragment>
    )
}

export default CommonSocial;
